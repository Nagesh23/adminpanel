import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { componentDeclarations, providerDeclarations } from './landing.common';
import { HeaderSidebarModule } from '../shared/widgets_module/header-sidebar/header-sidebar.module';
import { MaterialModule } from '../shared/widgets_module/material/material.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [...componentDeclarations],
    imports: [
        CommonModule, HeaderSidebarModule, MaterialModule,
        LandingRoutingModule, IonicModule
    ],
    providers: [...providerDeclarations]
})

export class LandingModule { }
