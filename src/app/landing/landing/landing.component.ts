import { Component, OnInit } from '@angular/core';
import { Platform, MenuController } from '@ionic/angular';
import { CommonService } from 'src/app/shared/core/services/common.service';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

    toggle: boolean = true;
    isDesktop: boolean;

    constructor(private _plt: Platform, public menuCtrl: MenuController,
        public comSer: CommonService) {}
    ngOnInit() {
        this.isDesktop = this._plt.is('desktop') || this._plt.is('electron');
    }
    toggleMenu() {
        this.menuCtrl.toggle(); //add this method to your button click function
    }
}
