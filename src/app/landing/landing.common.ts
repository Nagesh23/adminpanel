import { Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { AuthLoginGuard } from '../shared/core/services/auth-login.guard';

export const componentDeclarations: any[] = [LandingComponent];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
    {
        path: '', component: LandingComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: 'src/app/feature-modules/home/home.module#HomePageModule', canLoad: [AuthLoginGuard] },
            { path: 'doctors', loadChildren: 'src/app/feature-modules/doctors/doctors.module#DoctorsPageModule', canLoad: [AuthLoginGuard] },
            { path: 'appointments', loadChildren: 'src/app/feature-modules/appointments/appointments.module#AppointmentsPageModule', canLoad: [AuthLoginGuard] }
        ]
    }
];

