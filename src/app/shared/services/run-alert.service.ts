import { Injectable } from '@angular/core';
import { OverlayService, PositionStrategy, PopupRef } from '../core/services/overlay.service';
import { ErrorState } from '../models/error_state';
import { AlertComponent } from '../widgets_module/alert/alert.component';

@Injectable()
export class RunAlertService {

    private _popupRef: PopupRef; 
    constructor(private _overlay: OverlayService) { }

    runAlert(error: ErrorState) {
        this._popupRef ? this._popupRef.closeDialog() : () => {};
        this._popupRef = this._overlay.open<AlertComponent, ErrorState>(AlertComponent, null, {
            width: '400px', height: '75px', positions: PositionStrategy.CENTER_TO_VIEW,
            disableClose: false, backdropClass: 'transparent', hasBackdrop: false
        }, null, error);
    }
}
