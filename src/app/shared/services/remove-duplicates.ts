export type GetIdentifier<T> = (value: T) => any;
export const defaultGetIdentifier: GetIdentifier<any> = (val) => val;
export function removeDuplicates<T>(
    collection: T[],
    getIdentifier: GetIdentifier<T> = defaultGetIdentifier
) {
    const identifierState: { [identifier: string]: boolean } = {};
    return collection.filter((value) => {
        const identifier = String(getIdentifier(value));
        // console.log('identifier = ', identifier, 'type of identifier is = ', typeof identifier);
        if (identifierState[identifier.trim()]) {
        // console.log('identifier exist = ', identifier);
            return false;
        }
        // identifier !== 'undefined' ? identifierState[identifier.trim()] = true : ()=> {};
        return identifier !== 'undefined' ? (identifierState[identifier.trim()] = true, true) : false;
        // identifierState[identifier] = true;
        // console.log('identifierState = ', identifierState);
        return true;
    });
}

