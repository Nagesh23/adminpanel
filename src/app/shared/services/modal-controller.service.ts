import { Injectable } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { ModalOptions, LoadingOptions } from '@ionic/core';

const DEFAULT_MODAL_CONFIG: ModalOptions = {
    component: null,
    componentProps: null,
    showBackdrop: true,
    backdropDismiss: true,
    cssClass: [],
    delegate: null,
    animated: true,
    mode: null,
    keyboardClose: false,
    id: null,
    enterAnimation: null,
    leaveAnimation: null
}
const DEFAULT_LOADING_CONFIG: LoadingOptions = {
    spinner: 'circles',
    message: undefined,
    cssClass: [],
    showBackdrop: false,
    duration: undefined,
    translucent: false,
    animated: true,
    backdropDismiss: false,
    mode: null,
    keyboardClose: true,
    id: null,
    enterAnimation: null,
    leaveAnimation: null,
}

@Injectable()
export class ModalControllerService {

    constructor(private _modalCont: ModalController, private _loaderCont: LoadingController) { }

    async create(modal_config: ModalOptions) {
        const modal: HTMLIonModalElement = await this._modalCont.create({
            ...DEFAULT_MODAL_CONFIG, ...modal_config
        });
        return modal;
    }
    async createLoader(loader_config?: LoadingOptions) {
        const loader: HTMLIonLoadingElement = await this._loaderCont.create({ ...DEFAULT_LOADING_CONFIG, ...loader_config });
        return loader;
    }
    dismissLoader(data?: any, role?: string, id?: string) {
        this._loaderCont.dismiss(data, role, id);
    }
    dismiss(data?: any) {
        this._modalCont.dismiss(data);
    }
}
