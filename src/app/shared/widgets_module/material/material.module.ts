import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule, MatCheckboxModule, MatPaginatorModule,
  MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatRadioModule, MatAutocompleteModule, 
  MatSnackBarModule, MatButtonModule, MatDialogModule, MatFormFieldModule, MatTooltipModule,
  MatExpansionModule, MatInputModule, MatProgressBarModule, MatChipsModule, 
  MatIconModule, MatRippleModule, MatTabsModule, MatSlideToggleModule, MatMenuModule, MatBadgeModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import {MatSidenavModule} from '@angular/material/sidenav'
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [ CommonModule, MatBadgeModule, FormsModule, ReactiveFormsModule, MatSidenavModule,
    MatProgressSpinnerModule, MatInputModule, MatPaginatorModule, MatFormFieldModule, MatRippleModule, MatMenuModule,
    MatCheckboxModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule, MatDialogModule,
    MatSelectModule, MatRadioModule, MatAutocompleteModule, MatSnackBarModule, MatTooltipModule, MatSlideToggleModule,
    MatExpansionModule, MatInputModule, MatProgressBarModule, MatChipsModule, MatTabsModule, DragDropModule,
    MatInputModule, MatChipsModule, MatIconModule, MatTabsModule, A11yModule
  ],
  declarations: [],
  exports: [ CommonModule, FormsModule, ReactiveFormsModule, MatProgressSpinnerModule, MatInputModule,
    MatCheckboxModule, MatSnackBarModule,
    MatPaginatorModule, MatTooltipModule, MatSidenavModule,
    MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatRadioModule, MatAutocompleteModule, MatFormFieldModule,
    MatButtonModule, MatDialogModule, MatExpansionModule, MatProgressBarModule, MatChipsModule, MatTabsModule,
    MatSlideToggleModule, MatMenuModule, DragDropModule, MatBadgeModule,
    MatDialogModule, MatExpansionModule, MatChipsModule, MatIconModule, MatTabsModule, A11yModule,
    MatSlideToggleModule, MatMenuModule],
})

export class MaterialModule { }
