import { Component, OnInit, Input } from '@angular/core';
import { PopupRef } from '../../core/services/overlay.service';
import { ErrorState } from '../../models/error_state';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

    @Input() popupRef: PopupRef;
    @Input() data: ErrorState;
    constructor() {}
    ngOnInit() {
        setTimeout(() => {
            this.popupRef.closeDialog();
        }, 3000);
    }
}
