import { NgModule } from '@angular/core';
import { AlertComponent } from './alert.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
    declarations: [AlertComponent],
    imports: [
        MaterialModule
    ],
    exports: [AlertComponent],
    entryComponents: [AlertComponent]
})
export class AlertModule { }
