import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/core/services/common.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    isLogin$: Observable<boolean>;
    loaderState$: Observable<boolean>;
    constructor(private _comSer: CommonService) {}
    ngOnInit() {
        this.loaderState$ = this._comSer.loaderState;
        this.isLogin$ = this._comSer.loginState; 
    }
}
