import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { NotificationModule } from 'src/app/feature-modules/notification/notification.module';

@NgModule({
    declarations: [HeaderComponent, SidebarComponent],
    imports: [
        MaterialModule, RouterModule, NotificationModule
    ],
    exports: [HeaderComponent, SidebarComponent]
})

export class HeaderSidebarModule { }
