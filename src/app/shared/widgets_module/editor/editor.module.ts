import { NgModule } from '@angular/core';
import { EditorRoutingModule } from './editor-routing.module';
import { MaterialModule } from '../material/material.module';
import { EditorComponent } from './editor/editor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
    declarations: [EditorComponent],
    imports: [
        MaterialModule, EditorRoutingModule, CKEditorModule
    ],
    exports: [EditorComponent]
})

export class EditorModule { }
