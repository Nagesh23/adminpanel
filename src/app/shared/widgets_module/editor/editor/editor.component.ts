import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditorComponent implements OnInit {

    @Input() value: string;
    @Input() placeholder: string;
    editor = ClassicEditor;
    constructor() { }
    ngOnInit() { }

}
