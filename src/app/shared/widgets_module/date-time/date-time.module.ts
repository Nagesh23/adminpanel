import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { DateTransformerPipe } from './date-transformer/date-transformer.pipe';
import { DatePipe } from '@angular/common';

export let dateFactory = () => (new DateTransformerPipe(new MomentDateAdapter('en-US')));

export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
    declarations: [ DatePickerComponent, DateTransformerPipe ],
    imports: [
        MaterialModule
    ],
    exports: [DatePickerComponent, DateTransformerPipe],
    providers: [
        { provide: DatePipe, useFactory: dateFactory },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
    ]
})
export class DateTimeModule { }
