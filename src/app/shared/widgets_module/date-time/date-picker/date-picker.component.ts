import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter } from '@angular/material';

@Component({
    selector: 'app-date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.scss'],
})
export class DatePickerComponent implements OnInit {

    @Input() placeholder: string;
    @Input() dateControl: FormControl = new FormControl();
    @Input() formats: { parseFormat: string, displayFormat: string } = { parseFormat: 'DD-MM-YYYY', displayFormat: 'DD-MM-YYYY' };
    @Input() set date2StartWith(dateString: string) {
        const parsedDate: moment.Moment = this._dateAdapter.parse(dateString, this.formats.parseFormat);
        this.dateControl ? this.dateControl.setValue(parsedDate) : this.dateControl = new FormControl(parsedDate);
    };
    @Output() datePicked: EventEmitter<string> = new EventEmitter<string>();
    constructor(private _dateAdapter: DateAdapter<moment.Moment>) { }
    ngOnInit() {}
    dateSelected(date: moment.Moment) {
        if (date) {
            const formattedDate = this._dateAdapter.format(date, this.formats.displayFormat);
            this.datePicked.emit(formattedDate);
        }
    }
}

/** 
 * ```ts
 * parseFormat is the current format of date string is being parsed.
 * displayFormat is the format of the date wants to display on the screen.
 */
