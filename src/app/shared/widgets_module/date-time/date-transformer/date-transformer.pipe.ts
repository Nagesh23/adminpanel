import { Pipe, PipeTransform } from '@angular/core';
import { DateAdapter } from '@angular/material';
import { Moment } from 'moment';

@Pipe({
    name: 'dateTransformer',
})
export class DateTransformerPipe implements PipeTransform {

    constructor(private _dateAdapter: DateAdapter<Moment>) {}
    transform(value: any, parseFormat?: string, displayFormat?: string): string {
        const parsedDate: Moment = this._dateAdapter.parse(value, parseFormat);
        console.log(parsedDate);
        
        const displayDate: string = this._dateAdapter.format(parsedDate, displayFormat);
        console.log(displayDate);
        
        return displayDate;
    }
}

/** 
 * ```ts
 * parseFormat is the current format of date string is being parsed.
 * displayFormat is the format of the date wants to display on the screen.
 */
