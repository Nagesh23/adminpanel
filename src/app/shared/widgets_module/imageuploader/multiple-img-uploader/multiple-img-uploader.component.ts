import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FileUploadService } from '../services/file-upload.service';
import { UploadFileModel } from 'src/app/shared/models/upload_file';
import { Subject } from 'rxjs';
import { mergeMap, map, take } from 'rxjs/operators';

@Component({
    selector: 'app-multiple-img-uploader',
    templateUrl: './multiple-img-uploader.component.html',
    styleUrls: ['./multiple-img-uploader.component.scss'],
    providers: [FileUploadService]
})
export class MultipleImgUploaderComponent implements OnInit {

    upload: Subject<[number, FormData]> = new Subject<[number, FormData]>();
    @Output() sendSelectedImages = new EventEmitter<UploadFileModel[]>();
    selectedFiles: UploadFileModel[] = [];
    constructor(private _progEventSer: FileUploadService) { }
    ngOnInit() { }
    uploadImage(event: any) {
        this.upload.pipe(
            mergeMap(([index, fd]: [ number, FormData]) => {
                return this._progEventSer.uploadImages(fd).pipe(map( ( value: string | number ) => ( [index, value] ) ));
            }),
            take(event.target.files.length),
        ).subscribe(([ index, value ]: [number, string | number]) => {
            if ( typeof value === 'number') { this.selectedFiles[index].progress = value; }
            else { this.selectedFiles[index].url =  value; }
        }, (error) => { }, () => { this.sendSelectedImages.emit(this.selectedFiles) });
        const files: FileList = <FileList>event.target.files;
        for (let i = 0; i < files.length; i++) {
            const fd = new FormData();
            fd.append("file", files[i]);
            fd.append("name", files[i].name);
            this.selectedFiles.push(new UploadFileModel());
            this.selectedFiles[this.selectedFiles.length - 1].name = files[i].name;
            this.upload.next([this.selectedFiles.length - 1, fd]);
        }
        event.target.value = null;
    }
    loadImage(event, i: number, name) {
        const reader = new FileReader();
        reader.onload = (event: any) => {
            const fileModel = new UploadFileModel();
            fileModel.result = event.target.result;
            fileModel.name = name;
        };
        reader.readAsDataURL(event.target.files[i]);
    }
}
