import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleImgUploaderComponent } from './multiple-img-uploader.component';

describe('MultipleImgUploaderComponent', () => {
  let component: MultipleImgUploaderComponent;
  let fixture: ComponentFixture<MultipleImgUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleImgUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleImgUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
