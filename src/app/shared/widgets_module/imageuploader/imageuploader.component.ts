import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
// import { BaseComponent } from '../../framework/BaseCompo';
// import { CommonServices } from '../core/common.service';
// import { ApiGenerator } from '../../AppModuleUtilities/AppModApiGen';
// import { TaskCode } from '../../AppModuleUtilities/AppModGlobals';
import { FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'imageuploader',
	templateUrl: './imageuploader.component.html',
	styleUrls: ['./imageuploader.component.css']
})
export class ImageuploaderComponent implements OnInit {

	selectedFile: File = null;
	responseImage: string;
	@Input() recievedImage: string | undefined;
	@Output() sendImageUrl = new EventEmitter<{ url: string }>();
	@Input() fileControl: FormControl;
	@Input() isDisable: boolean;

	// constructor(protected comSer: CommonServices) {
	// 	super(comSer);
	// }
	ngOnInit() {}
	// uploadImage(event) {
	// 	this.selectedFile = <File>event.target.files[0];
	// 	this.isRequired();
	// 	const fd = new FormData();
	// 	fd.append('imageFile', this.selectedFile, this.selectedFile.name);
	// 	this.downloadData( ApiGenerator.callUploadingPhyImage(fd));
	// 	event.target.value = null;
	// }
	// isRequired() {
	// 	if (this.fileControl) {
	// 		const reader = new FileReader();
	// 		reader.readAsDataURL(this.selectedFile);
	// 		reader.onload = () => {
	// 			this.fileControl.setValue(reader.result);
	// 		};
	// 	}
	// }
	// onResponseReceived(taskCode: TaskCode, response: any) {
    //     const isSuccess = super.onResponseReceived(taskCode, response);
    //     switch (taskCode) {
	// 		case TaskCode.POST_PHYSICIAN_IMAGE_CODE:
	// 			if (!response.error) {
	// 				this.responseImage = response.data;
	// 				this.sendImageUrl.emit({ url: response.data });
	// 			}
    //         break;
    //     }
	// 	return isSuccess;
    // }
}
