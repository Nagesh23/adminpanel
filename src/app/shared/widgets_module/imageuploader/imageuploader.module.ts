import { NgModule } from '@angular/core';
import { ImageuploaderComponent } from './imageuploader.component';
import { MultipleImgUploaderComponent } from './multiple-img-uploader/multiple-img-uploader.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    MaterialModule
  ],
  declarations: [ImageuploaderComponent, MultipleImgUploaderComponent],
  exports: [ImageuploaderComponent, MultipleImgUploaderComponent]
})
export class ImageuploaderModule { }
