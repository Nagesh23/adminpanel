import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { PostUploadResponse } from "../../../models/upload_file";
import { HttpURLS } from 'src/app/shared/models/api_urls';

@Injectable()

export class FileUploadService {
    constructor(private _httpReq: HttpRequestService) { }
    uploadImages(fd: FormData): any {
        return this._httpReq.httpUploadLDRequest<PostUploadResponse>(PostUploadResponse, "POST", HttpURLS.UPLOAD_FILE,
            fd, { reportProgress: true, headers: new HttpHeaders({}) }).pipe(
                map((res: number | PostUploadResponse) => {
                    if (typeof res === 'number') {
                        return res;
                    } else {
                        if (!(<PostUploadResponse>res).error) { return (<PostUploadResponse>res).data; }
                        else { this._httpReq.runAlert({ status: true, title: (<PostUploadResponse>res).message, description: 'Please try again...' }); }
                    }
                })
            );
    }
}