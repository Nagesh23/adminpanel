import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { CitiesPredictionComponent } from './cities-prediction/cities-prediction.component';
import { AgmCoreModule } from "@agm/core";
@NgModule({
  declarations: [CitiesPredictionComponent],
  imports: [
    AgmCoreModule,
    MaterialModule
  ],
  exports: [CitiesPredictionComponent]
})
export class PlacesMapsModule { }
