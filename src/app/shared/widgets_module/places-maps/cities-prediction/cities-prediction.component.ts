/// <reference types="googlemaps" />
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { Address } from 'src/app/shared/models/address';

@Component({
    selector: 'app-cities-prediction',
    templateUrl: './cities-prediction.component.html',
    styleUrls: ['./cities-prediction.component.scss'],
})
export class CitiesPredictionComponent implements OnInit, OnDestroy {

    private _address: Address;
    private _autoCompSubsn: Subscription;
    predictions: google.maps.places.AutocompletePrediction[];
    @Input() autoCompCT: FormControl;
    @Input() set address(address: Address) {
        this._address = address ? (this.autoCompCT.setValue(address.description), address) : new Address();
    }
    @Output() emitAddress: EventEmitter<Address> = new EventEmitter<Address>();
    autoCompSer: google.maps.places.AutocompleteService = new google.maps.places.AutocompleteService();
    constructor() { }
    ngOnDestroy() {
        this._autoCompSubsn.unsubscribe();
    }
    ngOnInit() {
        this._autoCompSubsn = this.autoCompCT.valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
            switchMap((value: string) => {
                if (value) this.loadCityPredictions(value);
                return of(value);
            })).subscribe((result: any) => {});
    }
    loadCityPredictions(query: string) {
        this.autoCompSer.getPlacePredictions(
            { input: query, types: ['(cities)'], componentRestrictions: { country: ['in'] } },
            (predictions: google.maps.places.AutocompletePrediction[], status: google.maps.places.PlacesServiceStatus) => {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    this.predictions = predictions;
                }
            }
        );
    }
    placeSelected(place: google.maps.places.AutocompletePrediction) {
        this.autoCompCT.setValue(place.description);
        const primaryText = place.structured_formatting.main_text;
        const split1 = primaryText.split(",");
        if (split1.length === 1) {
            this._address.addressLine2 = primaryText;
            this._address.city = primaryText;
        } else {
            if (split1.length > 0) {
                this._address.city = split1[split1.length - 1].trim();
            }
            if (split1.length > 1) {
                let stringBuilder = '';
                for (let x = 0; x < split1.length - 1; x++) {
                    stringBuilder = stringBuilder + split1[x].trim() + ',';
                }
                stringBuilder = stringBuilder.substring(0, stringBuilder.length);
                this._address.addressLine2 = stringBuilder;
            }
        }
        const secondaryText = place.structured_formatting.secondary_text;
        const split = secondaryText.toString().split(",");
        if (split.length > 0) {
            this._address.country = split[split.length - 1].trim();
        }
        if (split.length > 1) {
            this._address.state = split[split.length - 2].trim();
        }
        if (split.length > 2) {
            this._address.city = split[0].trim();
        }
        if (this._address.addressLine1) {
            this._address.description = this._address.addressLine1 + this._address.addressLine2;
        } else {
            this._address.description = this._address.addressLine2;
        }
        this._address.latitude = '';
        this._address.longitude = '';
        this._address.fullAddress = this._address.description + ', ' + this._address.city + ', ' + this._address.state;
        this.emitAddress.emit(this._address);
    }
}
