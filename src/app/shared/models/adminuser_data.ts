import { Type } from 'class-transformer';
import { BaseResponse } from './base_response';

export class AdminUserResponse extends BaseResponse {
    // @Type(() => AdminUserData)
    data: AdminUserData;
}

export class AdminUserData {
    created: number;
    updated: number;
    updatedByRole: number;
    version: number;
    firstName: string;
    lastName: string;
    fullName: string;
    emailId: string;
    gender: string;
    imageUrl: string;
    thumbnailImageUrl: string;
    phoneNumber: string;
    adminUserRoleType: number;
    authorised: boolean;
    pin: number;
    adminUserId: string;
}
