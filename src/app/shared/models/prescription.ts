import { BaseResponse } from './base_response';
import { Type } from 'class-transformer';
import { SnomedSearch } from './snomed_search';
import { Vital } from './vitals';
import { ClinicInfoList } from './clinic_info';
import { Patient } from './patient_data';
import { PhysicianData } from './physician_data';
import { AppointmentData } from './appointment_data';
import { Medicine } from './medicine';
import { RefferencePhysician } from './reference_physician';
import { DiseaseEntity } from './lab_results_entities';
import { SocialHistory } from './social_history';

export class PrescriptionResponse extends BaseResponse {
    @Type(() => Prescription)
    data: Prescription[];
}
export class AddPrescirpitonPost_S extends BaseResponse {
    @Type(() => Prescription)
    data: Prescription;
}
export class Prescription {

    constructor(chifComplaints: SnomedSearch[], diagnosis: SnomedSearch[],
    allergies: SnomedSearch[], investigations: SnomedSearch[], vitals: Vital[], medications: Medicine[]) {
        this.chiefComplaintsList = chifComplaints;
        this.diagnosisList = diagnosis;
        this.allergiesList = allergies;
        this.furtherAnalysisList = investigations;
        this.vitals = vitals;
        this.medications = medications;
    }

    caseStudy: string = '<p></p>';
    patientName: string;
    patientHeight: string;
    patientWeight: string;
    prescriptionDate: string;
    physicianId: string;
    patientId: string;
    @Type(() => SnomedSearch)
    allergiesList: SnomedSearch[];
    @Type(() => SnomedSearch)
    furtherAnalysisList: SnomedSearch[];
    @Type(() => Vital)
    vitals: Vital[];
    @Type(() => SnomedSearch)
    diagnosisList: SnomedSearch[];
    @Type(() => Medicine)
    medications: Medicine[];
    doctorNote: string  = '<p></p>';
    patientNote: string = '<p></p>';
    followUpRequired: boolean;
    followUpDate: string;
    clinicId: string;
    queueId: string;
    @Type(() => SnomedSearch)
    chiefComplaintsList: SnomedSearch[];
    clinicData: ClinicInfoList;
    prescriptionId: string;
    patientType: number;
    prescriptionNo: string;
    created: number;
    updated: number;
    version: number;
    @Type(() => SnomedSearch)
    medicalHistory: SnomedSearch[];
    @Type(() => SnomedSearch)
    surgicalHistory: SnomedSearch[];
    referredPhysician: RefferencePhysician;
    patientBmiGlobal: number;    // Nagesh Added
    patientBsaDuboisGlobal: number;
    patientBsaMostellerGlobal: number;
    @Type(() => BillData)
    billData: BillData;
    @Type(() => Patient)
    patientData: Patient;
    @Type(() => PhysicianData)
    physicianData: PhysicianData;
    billGenerated: boolean;
    sessionStartTime: number;
    writePrescStartTime: number;
    reviewPrescStartTime: number;
    sessionEndTime: number;
    reportsData: DiseaseEntity[] = [];
    socialHistory: SocialHistory[];
    languageCode: string = 'en';
    patientAdvice: string[] = [];
    followupAppointment: AppointmentData;
    templeteName: string;
    templeteId: string;

    /** CUSTOM FIELDS  */
    isEvenSnackCol: boolean;
}
export class BillData {
    @Type(() => AmountDataList)
    amountData: AmountDataList[] = [];
    visitDate: string;
    prescriptionNumber: string;
    physicianId: string;
    clinicId: string;
    patientId: string;
    billDate: string;
    billNumber: string;
    discount = 0;
    paidAmount: number;
    totalAmount: number;
    cgst = 0;
    sgst = 0;
    igst = 0;
    paymentStatus: number;
    paymentMethod: number;
    // roundOff: number = 0;
    remainingAmount: number;
    description: string;
    subTotal: number;
    summary: string;
    comments: string;
}
export class AmountDataList {
    amount: number;
    description: string;
    appliedRule: string;
    visitPurposeId: string;
    isDropDownOpen: boolean;
    isTaxable: boolean;
}
