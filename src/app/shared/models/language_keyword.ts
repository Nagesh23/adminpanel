import { BaseResponse } from "./base_response";
import { Type } from "class-transformer";

export class LanguageKeywordModel extends BaseResponse {
    @Type(() => LanguageKeyword)
    data: LanguageKeyword[];
}

export class LanguageKeyword {
    created: any;
    updated: any;
    version: number;
    translatedData: string;
    keyword: string;
    languageCode: string;
    keywordId: string;
    languageKeywordsId: string;
}
