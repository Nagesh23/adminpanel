export enum Days_Weeks_Months {
    DAYS = 'days',
    WEEKS = 'weeks',
    MONTHS = 'months'
}
