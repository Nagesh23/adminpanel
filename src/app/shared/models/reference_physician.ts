import {Type} from "class-transformer";

export class RefferencePhysicianResponse {
    @Type(() => RefferencePhysician)
    data: RefferencePhysician[];
}

export class RefferencePhysician {
    created: number;
    updated: number;
    createdByRole: number;
    updatedByRole: number;
    version: number;
    physicianName: string;
    specialisationName: string;
    qualificationName: string;
    city: string;
    physicianId: string;
    referencePhysicianId: string;
    referedDate: string;
    // custom
    flow: number;
}
