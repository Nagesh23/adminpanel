export class PhysicianAuthoriseModel {
    physicianId: string;
    authorise: boolean;
}