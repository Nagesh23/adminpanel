import { BaseResponse } from './base_response';
import { Type } from 'class-transformer';

export class VitalResponse extends BaseResponse {
    @Type(() => Vital)
    data: Vital[];
}

export class Vital {
    vitalId: string;
    vitalName: string;
    shortCode: string;
    unit: string;
    minValue: number;
    maxValue: number;
    showGraph: boolean;
    graphType: number;
    showWithVitals: boolean;
    value: number;
    validate = false;
    bp: string;
    colorCode: string;
}
