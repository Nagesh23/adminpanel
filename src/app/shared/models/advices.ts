import { InjectionToken } from "@angular/core";

export class Advice {
    adviceId: string;
    langVrs?: LanguageVr[];
    hi?: string;
    mr?: string;
    en?: string;
}
export class LanguageVr {
    code: string;
    value: string;
}

const adviceList: Advice[] = [{
        adviceId: '123',
        hi: 'तंबाकू के सभी प्रकार के उपभोग को बंद करें।',
        en: 'Strictly Stop Tobacco',
        mr: 'तंबाखुचे सर्व प्रकारचे सेवन बंद करणे',
    }, {
        adviceId: '124',
        en: 'Do Triball Excercise',
        mr: 'दिर्घश्वसनाचे व्यायाम दिलेल्या यंत्राच्या मदतीने करणे.',
        hi: 'दिए गए उपकरणों के साथ गहरी साँस लेने का व्यायाम करें।',
    }, {
        adviceId: '125',
        en: 'Do Gargles with luke warm water',
        mr: 'कोमट पाण्याच्या गुळण्या करणे.',
        hi: 'गुनगुने पानी से गरारे करें।',
    }, {
        adviceId: '126',
        en: 'Brisk walking daily (30 to 45 mints)',
        mr: 'रोज जमेल तेवढ्या गतीने न दमता सुमारे ३० - ४५ मिनिटे चालणे.',
        hi: 'लगभग 30 से 45 मिनट तक तेज गति से चलने का व्यायाम करे।'
    }, {
        adviceId: '127',
        en: 'Take Steam Inhalation',
        mr: 'दिवसातून ३ - ४ वेळा वाफ घेणे.',
        hi: 'दिन में 3 - 4 बार भाप लें।'
    }, {
        adviceId: '128',
        en: 'Drink Boiled Water',
        mr: 'पाणी उकळून थंड करून पिणे.',
        hi: 'उबला हुआ पानी पिएं।'
    }, {
        adviceId: '129',
        en: 'Daily Excercise',
        mr: 'दररोज नियमित व्यायाम करणे - योगा प्राणायाम .',
        hi: 'प्रतिदिन नियमित रूप से व्यायाम करना - योग प्राणायाम।',
    }, {
        adviceId: '130',
        en: 'Salt Restricted Diet',
        mr: 'जेवणात मिठाचे प्रमाण कमी करणे.',
        hi: 'भोजन में नमक की मात्रा कम करें।',
    }, {
        adviceId: '131',
        en: 'Sweet Restriction',
        mr: 'गोड पदार्थ बंद करणे.',
        hi: 'मीठे पदार्थो का सेवन बंद करे।',
    }, {
        adviceId: '132',
        en: 'Stop Smoking/ Drinking Alcohol',
        mr: 'धूम्रपान/मद्यपान बंद करणे.',
        hi: 'धूम्रपान/शराब तथा अन्य नशीले पदार्थो का सेवन बंद करे।'
    }
];

export const PATIENTADVICES = new InjectionToken<Advice[]>('ADVICES', 
    { factory: () => adviceList }
);
