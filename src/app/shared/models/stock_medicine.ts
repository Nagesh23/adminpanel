import { Type } from 'class-transformer';

export class StockMedicine {
    pharmacyId: string;
    @Type(() => MedicineData)
    medicineData: MedicineData;
    purchesId: string;
    mainPurchesId: string;
    totalQuantity: number;
    batchNumber: string;
    barCode: string;
    pharmacyStockId: string;
    created: number;
    updated: number;
}



export class MedicineData {
    medicineId: string;
    medicineName: string;
    expDateString: string;
    pkgDateString: string;
    expDate: string;
    pkgDate: string;
    unit: number;
    mrp: number;
    costPrice: number;
    cgst: number;
    sgst: number;
    igst: number;
}


