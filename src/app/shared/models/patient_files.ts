import { BaseResponse } from './base_response';
import { Type } from 'class-transformer';

export class PatientFilesResponse extends BaseResponse {
    @Type(() => PatientFile)
    data: PatientFile[];
}

export class PatientFile {
    created: number;
    updated: number;
    createdBy: string;
    updatedBy: string;
    createdByName: string;
    updatedByName: string;
    version: number;
    patientId: string;
    clinicId: string;
    @Type(() => FileType)
    fileTypeData: FileType;
    title: string;
    @Type(() => FileUrl)
    files: FileUrl[];
    patientFileId: string;
}
export class FileUrl {
    fileUrl: string;
    thumbnailUrl: string;
    getFileUrl(): string{
        if (this.fileUrl) {
            if (this.fileUrl.lastIndexOf('.pdf') > 0){
                return 'https://cdn4.iconfinder.com/data/icons/file-extensions-1/64/pdfs-512.png';
            }
        }
        return this.fileUrl;
    }
}

export class FileTypesResponse extends BaseResponse {
    @Type(() => FileType)
    data: FileType[];
}

export class FileType {
    created: number;
    updated: number;
    createdBy: string;
    updatedBy: string;
    createdByName: string;
    updatedByName: string;
    version: number;
    name: string;
    code: number;
    subTypes: string[];
    imageUrl: string;
    fileTypeId: string;
}
