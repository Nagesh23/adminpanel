import { BaseResponse } from "./base_response";
import { Type } from "class-transformer";

export class CommentsResponse extends BaseResponse {
    @Type(() => Comment)
    data: Comment[];
}

export class Comment {
    created: number;
    updated: number;
    createdBy: string;
    updatedBy: string;
    createdByName: string;
    updatedByName: string;
    createdByRole: number;
    updatedByRole: number;
    version: number;
    physicianId: string;
    clinicId: string;
    comment: string;
    medicinesCommentId: string;
}