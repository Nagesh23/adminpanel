import { BaseResponse } from './base_response';
import { Type } from "class-transformer";

export class Refer2PhyResponse extends BaseResponse {
    @Type(() => Refer2Phy)
    data: Refer2Phy[];
}

export class Refer2Phy {
    created: number;
    updated: number;
    createdByRole: number;
    updatedByRole: number;
    version: number;
    physicianName: string;
    specialisationName: string;
    qualificationName: string;
    city: string;
    physicianId: string;
    referencePhysicianId: string;
    referedDate: string;
}