import { BaseResponse } from './base_response';
import { Address } from './address';
import { ClinicInfoList } from './clinic_info';
import { Type } from "class-transformer";


export class PhysiciansQuoClinicRes extends BaseResponse {
    data: PhysiciansQuoClinic;
}

export class PhysiciansQuoClinic {
    clinicId: string;
    physiciansList: PhysicianData[];
}

export class PhysiciansResponse extends BaseResponse {
    // @Type(() => PhysicianData)
    data: PhysicianData[];
    totalCount: number;
}
export class PhysicianResponse extends BaseResponse {
    // @Type(() => PhysicianData)
    data: PhysicianData;
}
export class PhysicianData {
    created: number;
    updated: number;
    version: number;
    registrationNumber: string;
    // @Type(() => PhyProfile)
    profile: PhyProfile;
    // @Type(() => Education)
    education: Education;
    // @Type(() => Certificates)
    certificates: Certificates;
    pin: number;
    authorised: boolean;
    declarationSigned: boolean;
    // @Type(() => Preferences)
    preferences: Preferences;
    letterHeadData: string[];
    physicianId: string;
    // @Type(() => ClinicInfoList)
    clinicInfoList: ClinicInfoList[];
    code: string;
    // @Type(() => Experience)
    experience: Experience;
    codeDate: string;
    pinUpdated = false;
    codeList: number[];
    medicineType: number;
    // custom
    authorisedName: string;
}

export class Preferences {
    medicineViewType: number;
    showFooterClinicName: boolean;
    showFooterRegistrationNumber: boolean;
    showFooterQualification: boolean;
    showMedicineGeneric: boolean;
    resultsOnPrescription: boolean;
    isChiefComplaintNextLine: boolean;
    showFooterAppNo: boolean;
    isInvestnOnNextLine: boolean;
    isDiagnosisOnNextLine: boolean;
    isVitalsOnNextLine: boolean;
    isLabResOnNextLine: boolean;
}

export class PhyProfile {
    firstName: string;
    lastName: string;
    fullname: string;
    emailId: string;
    gender: string;
    dateOfBirth: string;
    // dob: Date; // custom field
    imageUrl: string;
    // @Type(() => Address)
    address: Address;
    phoneNumber: string;
    alternatePhoneNumber?: any;
    aboutMe?: string;

    // custom fields
    phySignUrl: string;
    phyRegNo: string;
    letterHead: string = '';
    letterHeadData: string[] = [];
    wordLtElert: boolean;
}

export class Education {
    // @Type(() => QualificationsList)
    qualificationsList: QualificationsList[];
    // @Type(() => SpecializationList)
    specializationList: SpecializationList[];
}

export class QualificationsList {
    created?: any;
    updated?: any;
    createdBy?: any;
    updatedBy?: any;
    version?: any;
    // @Type(() => QualificationData)
    qualificationData: QualificationData;
    // @Type(() => CollegeData)
    collegeData: CollegeData;
}

export class SpecializationModel extends BaseResponse {
    // @Type(() => SpecializationList)
    data: SpecializationList[];
}

export class SpecializationList {
    created: any;
    updated: any;
    createdBy?: any;
    updatedBy?: any;
    version: number;
    specialization: string;
    imageUrl: string;
    selectedImageUrl: string;
    snomedId?: any;
    physiciansSpecializationId: string;

    // custom field
    booleanValue = false;
}

export class QualificationData {
    created: any;
    updated: any;
    version: number;
    qualificationName: string;
    qualificationId: string;
    verified: boolean;
    creatorType: number;
}

export class CollegeData {
    created?: any;
    updated?: any;
    createdBy?: any;
    updatedBy?: any;
    version?: any;
    name: string;
    // @Type(() => Address)
    address: Address;
    fromYear: number;
    toYear: number;
}

export class QualificationModel extends BaseResponse {
    // @Type(() => QualificationData)
    data: QualificationData[];
}

export class Certificates {
    // @Type(() => NameVsSpecializationCertiUrl)
    nameVsSpecializationCertiUrl: NameVsSpecializationCertiUrl;
    letterHeadUrl?: any;
    digitalSignatureUrl: string;
}

export class NameVsSpecializationCertiUrl {
}

export class NameVsSpecialisation {
    key: string;
    url: string;
}

export class ClinicData {
    name: string;
    // @Type(() => Address)
    clinicAddress: Address;
    logo: string;
    clinicId: string;
    fullAddress: string;
}

export class WorkPlace {
    experience: number;
    // @Type(() => Date)
    startDate: Date;
    // @Type(() => Date)
    endDate: Date;
    startMonthYear: string;
    endMonthYear: string;
    currentlyWorking: boolean;
    // @Type(() => ClinicData)
    clinicData: ClinicData;

    // custom fields
    stMonth: number;
    stYear: number;
    endMonth: number;
    endYear: number;

    setStartMonth(startMonth) {
        this.stMonth = startMonth;
        this.startMonthYear = startMonth < 10 ? '0' + this.stMonth + '-' + this.stYear : this.stMonth + '-' + this.stYear;
    }
    setStartYear(startYear) {
        this.stYear = startYear;
        this.startMonthYear = this.stMonth < 10 ? '0' + this.stMonth + '-' + this.stYear : this.stMonth + '-' + this.stYear;
    }
    setEndMonth(endMonth) {
        this.endMonth = endMonth;
        this.endMonthYear = endMonth < 10 ? '0' + this.endMonth + '-' + this.endYear : this.endMonth + '-' + this.endYear;
    }
    setEndYear(endYear) {
        this.endYear = endYear;
        this.endMonthYear = this.endMonth < 10 ? '0' + this.endMonth + '-' + this.endYear : this.endMonth + '-' + this.endYear;
    }
}

export class Experience {
    totalExperience: number;
    diplayTotalExperience: string;
    lastExpUpdated: number;
    // @Type(() => WorkPlace)
    workPlaces: WorkPlace[];
}
