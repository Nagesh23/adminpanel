import { Type } from 'class-transformer'
import { Constituent } from './search_medicine';
import { BaseResponse } from './base_response';

export class Medicine {

    medicineId: string;
    medicineName: string;
    dose: number;
    doseType: string;
    @Type(() => FrequencyData)
    frequencies: FrequencyData[] = [];
    @Type(() => Constituent)
    constituents: Constituent[];
    pharmacyStockId: string;
    batchNumber: string;
    expDateString: string;
    pkgDateString: string;
    vaccineId: string;
    imageUrl: string;
    painType = 1;
    medicineInfo: MedicineInfo = null;
    doseTypeView: number = 0;
    medicineType: number;
    medicineAction: number;

    constructor(frequencies: FrequencyData[], constituents: Constituent[]) {
        this.frequencies = frequencies;
        this.constituents = constituents;
    }
}
export class FrequencyData {
    morning: boolean; // checkbox of morning
    noon: boolean; // checkbox of noon
    evening: boolean; // checkbox of evening
    night: boolean; // checkbox of night
    afterMeals: boolean;
    morningDose = 0;
    noonDose = 0;
    eveningDose = 0;
    nightDose = 0;
    frequency: number; // no of days 
    comments: string;
    sosId: string;
    sosString: string;
    duration: number;
    days_months_weeks: string;
}


/**
 * Medicine Info
 */

export class MedicineInfoResponse extends BaseResponse {
    @Type(() => MedicineInfo)
    data: MedicineInfo;
}

export class MedicineInfo {
    created: number;
    updated: number;
    version: number;
    medicine_id: string;
    name: string;
    price: number;
    standardUnits: number;
    packageForm: string;
    manufacturer: string;
    form: string;
    size: string;
    score: number;
    @Type(() => Constituent)
    constituents: Constituent[];
    @Type(() => Schedule)
    schedule: Schedule;
    @Type(() => Pregnancy)
    pregnancy: Pregnancy;
    @Type(() => Lactation)
    lactation: Lactation;
    @Type(() => Component)
    components: Component[];
    @Type(() => Interactions)
    interactions: Interactions;
    medicine__id: string;
}

// export class Constituent { // for global constituent
//     name: string;
//     strength: string;
// }

export class Schedule { // for global schedule
    category: string;
    label: string;
}

export class Pregnancy { // for global pregnancy
    category: string;
    label: string;
    description: string;
}

export class Lactation { // for global lactation
    category: string;
    label: string;
    description: string;
}

export class Component { // for global component
    name: string;
    pregnancy_category: string;
    lactation_category: string;
    instructions: string;
    faqs: string;
    side_effects: string;
    how_it_works: string;
    therapeutic_class: string;
    used_for: string;
    strength: string;
    schedule: string;
    alcohol_interaction_description: string;
}

export class ColorCode {
    label: string;
    text: string;
}

export class IntPregnancy { // for interactions pregnacny
    tag: string;
    description: string;
    label: string;
    show_alert: boolean;
    @Type(() => ColorCode)
    color_code: ColorCode;
}

export class Food { // for interactions food
    tag: string;
    description: string;
    label: string;
    show_alert: boolean;
    @Type(() => ColorCode)
    color_code: ColorCode;
}

export class IntLactation { // for interactions lactation
    tag: string;
    description: string;
    label: string;
    show_alert: boolean;
    @Type(() => ColorCode)
    color_code: ColorCode;
}

export class Alcohol { // for interactions alcohal
    tag: string;
    description: string;
    label: string;
    show_alert: boolean;
    @Type(() => ColorCode)
    color_code: ColorCode;
}

export class Interactions { // for global interactions
    @Type(() => IntPregnancy)
    pregnancy: IntPregnancy;
    food: Food;
    @Type(() => IntLactation)
    lactation: IntLactation;
    @Type(() => Alcohol)
    alcohol: Alcohol;
}


/**
 * Recommended medicine model
 */

export class RecommendedMedicineResponse extends BaseResponse {
    @Type(() => Medicine)
    medication: Medicine;
    patientCurrentMedicinesCount: number;
    patientCronicMedicinesCount: number;
    physicianRecommendedMedicinesCount: number;
}


/** Medicine model from backend */

// Medicine Model {
// private String medicineId;
// private String medicineName;
// private List<MedincineComponent> constituents;

// private String dose;
// private String doseType;

// private List<MedicineFrequencyPojo> frequencies;

// private String pharmacyStockId;
// private String batchNumber;
// private String expDateString;
// private String pkgDateString;
// private String vaccineId;
// private String imageUrl;
// private Integer painType;
// private Integer medicineAction;
// private Integer doseTypeView;
// private Integer medicineType;
// }


// Medicine Components Model {
// private String name;
// private String pregnancy_category;
// private String lactation_category;
// private String instructions;
// private String faqs;
// private String side_effects;
// private String how_it_works;
// private String therapeutic_class;
// private String used_for;
// private String strength;
// private String schedule;
// private String alcohol_interaction_description;
// }

// Medicine Frequency Model {
// private boolean morning;
// private boolean noon;
// private boolean evening;
// private boolean night;

// private boolean afterMeals;

// private float morningDose;
// private float noonDose;
// private float eveningDose;
// private float nightDose;

// private int frequency;

// private String sosId;
// private String sosString;

// private String comments;
// }




    // medicineId: string;
    // medicineName: string;
    // dose: number;
    // doseType: string;
    // frequencies: FrequencyData[] = [];
    // @Type(() => Constituent)
    // constituents: Constituent[];
    // pharmacyStockId: string;
    // batchNumber: string;
    // expDateString: string;
    // pkgDateString: string;
    // vaccineId: string;
    // imageUrl: string;
    // painType = 1;
    // medicineInfo: MedicineInfo = null;
    // doseTypeView: number = 0;
    // medicineType: number;
    // medicineAction: number;

    // morning: boolean; // checkbox of morning
    // noon: boolean; // checkbox of noon
    // evening: boolean; // checkbox of evening
    // night: boolean; // checkbox of night
    // afterMeals: boolean;
    // morningDose = 0;
    // noonDose = 0;
    // eveningDose = 0;
    // nightDose = 0;
    // frequency: number; // no of days 
    // comments: string;
    // sosId: string;
    // sosString: string;