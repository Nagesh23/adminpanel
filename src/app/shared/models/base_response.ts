export class BaseResponse {
	error: boolean;
	message: string;
	code: string;
}
export class Response extends BaseResponse {
	data: any;
}
