import { BaseResponse } from './base_response';

export class PostUploadResponse extends BaseResponse {
    data: string;
    thumbnailUrl: string;
}

export class UploadFileModel {
    url: string;
    name: string;
    result: any;
    isSelected = false;
    progress: number;
}
