import { BaseResponse } from "./base_response";
import { Type } from "class-transformer";
import { PhysicianData } from "./physician_data";
import { Patient } from "./patient_data";
import { AppointmentData } from "./appointment_data";

export class QueueResponse extends BaseResponse {
    @Type(() => PatientInQueue)
    data: PatientInQueue[];
}
export class PatientInQueue {
    @Type(() => AddPatientToQueueData)
    queueDetails: AddPatientToQueueData;
    @Type(() => Patient)
    patientDetails: Patient;
    @Type(() => PhysicianData)
    physicianDetails: PhysicianData;
    @Type(() => AppointmentData)
    appointmentDetails: AppointmentData;
}
export class AddPatientToQueueData {
    clinicId: string;
    patientId: string;
    physicianId: string;
    appointmentTime: any;
    createdBy: string;
    verified = true;
    queueId: string;
    fileCode: string;
    waitTime: string;
    appointmentId: string;
    patientQueueStatus: number;
    removalTime: string;

    constructor(clinicId: string, patientId: string, physicianId: string, verified: boolean, appointmentTime: any) {
        this.clinicId = clinicId;
        this.patientId = patientId;
        this.physicianId = physicianId;
        this.verified = verified;
        this.appointmentTime = appointmentTime;
    }
}
