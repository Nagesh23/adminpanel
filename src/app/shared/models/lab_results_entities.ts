import { InjectionToken } from "@angular/core";
import { Type } from "class-transformer";

export class InvestigationType {
    investName: string;
	investID: ActiveInvestType | string;
	@Type(() => IRDiseaseGP)
    irDiseaseGP: IRDiseaseGP[];
}
export class IRDiseaseGP {
    diseaseName: string;
    diseaseID: IRDiseaseGPID | string;
	@Type(() => DiseaseEntity)
    diseaseEntity: DiseaseEntity[];
}
export class DiseaseEntity {
    name: string;
    value: string;
    id: string;
    diseaseID: string;
    investID: string;
    specimen: string;
    useCount: number;
    dataType: ValidationType | number;
    expectedBoolean: boolean;
    minRangeCI: number;
    maxRangeCI: number;
    unitsCI: string;
    minRangeSI: number;
    maxRangeSI: number;
	unitsSI: string;
	sequenceId?: number;
}
export enum ActiveInvestType {
    LAB_RESULTS = 'LAB_ID',
    CARDIOLOGY = 'CARDIOLOGY_ID',
	CHEST_RES = "CHEST_ID",
	URINE_RESULTS = "URINE_ID"
}
export enum IRDiseaseGPID {
    DIABETES = 'DIABETES',
    THYROID = 'THYROID',
    KIDNEY = 'KIDNEY',
    LIPID_PANEL = 'LIPIDPANEL',
    HEMOGRAM = 'HEMOGRAM',
    LIVER = 'LIVER',
    HIV_TEST = 'HIVTEST',
	INFECTION = 'INFECTION',
	URINE_R = 'URINERANDOM',
	URINE_5 = 'URINE5'
}

export enum ValidationType {
    _NUMBER = 1,
    _BOOLEAN = 2,
    _STRING = 3
}

export const results: InvestigationType[] = [
	{
		investName: "Laboratory",
		investID: "LAB_ID",
		irDiseaseGP: [
			{
				diseaseName: "Diabetes",
				diseaseID: "DIABETES",
				diseaseEntity: [{
						id: "(PPBSL)Glucose2hpostprandial",
						investID: "LAB_ID",
						diseaseID: "DIABETES",
						name: "(PPBSL) Glucose: 2-h postprandial",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0,
						maxRangeCI: 140,
						unitsCI: "mg/dL",
						minRangeSI: 0,
						maxRangeSI: 7.8,
						unitsSI: "mmol/L",
						sequenceId: 2
					},{
						id: "(BSL)GlucoseRandom",
						investID: "LAB_ID",
						diseaseID: "DIABETES",
						name: "(BSL) Glucose: Random",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 79,
						maxRangeCI: 160,
						unitsCI: "mg/dL",
						minRangeSI: 0,
						maxRangeSI: 7.8,
						unitsSI: "mmol/L",
						sequenceId: 3
					},{
						id: "(FBSL)GlucoseFasting",
						investID: "LAB_ID",
						diseaseID: "DIABETES",
						name: "(FBSL) Glucose: Fasting",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 70,
						maxRangeCI: 105,
						unitsCI: "mg/dL",
						minRangeSI: 3.9,
						maxRangeSI: 5.8,
						unitsSI: "mmol/L",
						sequenceId: 1
					},
					{
						id: "(HbA1c)HemoglobinA1c",
						investID: "LAB_ID",
						diseaseID: "DIABETES",
						name: "(HbA1c) Hemoglobin A1c",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 4.7,
						maxRangeCI: 8.5,
						unitsCI: "%",
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 4
					}
				]
			},
			{
				diseaseName: "Hemogram (CBC)",
				diseaseID: "HEMOGRAM",
				diseaseEntity: [{
						id: "(MCV)Meancorpuscularvolume",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "(MCV) Mean corpuscular volume",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 80,
						maxRangeCI: 100,
						unitsCI: "fL",
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 5
					},
					{
						id: "HemoglobinM",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "Hemoglobin (For Male)",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 14,
						maxRangeCI: 17,
						unitsCI: "g/dl",
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 1
					},
					{
						id: "HemoglobinF",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "Hemoglobin (For Female)",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 12,
						maxRangeCI: 16,
						unitsCI: "g/dl",
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 2
					},
					{
						id: "Plateletcount",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "Platelet count",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 150,
						maxRangeCI: 350,
						unitsCI: "x 10^3/mcL",
						minRangeSI: 150,
						maxRangeSI: 350,
						unitsSI: "x 10^9 /L",
						sequenceId: 6
					},
					{
						id: "RBCcount",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "RBC count",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 4.2,
						maxRangeCI: 5.9,
						unitsCI: "x 10^6 cells/mcL",
						minRangeSI: 4.2,
						maxRangeSI: 5.9,
						unitsSI: "x 10^12 cells/L",
						sequenceId: 4
					},
					{
						id: "WBCcount",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "WBC count",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 4.5,
						maxRangeCI: 11,
						unitsCI: "x 10^3 cells/mcL",
						minRangeSI: 4.5,
						maxRangeSI: 11,
						unitsSI: "x 10^9 cells/L",
						sequenceId: 3
					},
					{
						id: "Lymphocytes",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "Lymphocytes",
						specimen: "null",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0.77,
						maxRangeCI: 4.5,
						unitsCI: "x 10^6 cells/mcL",
						minRangeSI: 0.77,
						maxRangeSI: 4.5,
						unitsSI: "x 10^9 cells/L",
						sequenceId: 7
					},
					{
						id: "Eosinophils",
						investID: "LAB_ID",
						diseaseID: "HEMOGRAM",
						name: "Eosinophils",
						specimen: "null",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0,
						maxRangeCI: 0.55,
						unitsCI: "x 10^8 cells/mcL",
						minRangeSI: 0,
						maxRangeSI: 0.55,
						unitsSI: "x 10^9 cells/L",
						sequenceId: 8
					}
				]
			},
			{
				diseaseName: "Kidney (Renal Profile)",
				diseaseID: "KIDNEY",
				diseaseEntity: [{
						id: "Creatinine",
						investID: "LAB_ID",
						diseaseID: "KIDNEY",
						name: "Creatinine",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0.7,
						maxRangeCI: 1.3,
						unitsCI: "mg/dL",
						minRangeSI: 61.9,
						maxRangeSI: 115,
						unitsSI: "mcmol/L",
						sequenceId: 3
					},
					{
						id: "(K+)Potassium",
						investID: "LAB_ID",
						diseaseID: "KIDNEY",
						name: "(K+) Potassium",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 3.5,
						maxRangeCI: 5,
						unitsCI: "mEq/L",
						minRangeSI: 3.5,
						maxRangeSI: 5,
						unitsSI: "mmol/L",
						sequenceId: 2
					},
					{
						id: "(Na+)Sodium",
						investID: "LAB_ID",
						diseaseID: "KIDNEY",
						name: "(Na+) Sodium",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 136,
						maxRangeCI: 145,
						unitsCI: "mEq/L",
						minRangeSI: 136,
						maxRangeSI: 145,
						unitsSI: "mmol/L",
						sequenceId: 1
					},
					{
						id: "(BUN)Ureanitrogen",
						investID: "LAB_ID",
						diseaseID: "KIDNEY",
						name: "(BUN) Urea nitrogen",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 8,
						maxRangeCI: 20,
						unitsCI: "mg/dL",
						minRangeSI: 2.9,
						maxRangeSI: 7.1,
						unitsSI: "mmol/L",
						sequenceId: 4
					}
				]
			},
			{
				diseaseName: "Lipid Profile",
				diseaseID: "LIPIDPANEL",
				diseaseEntity: [{
						id: "(HDLC)CholesterolHighdensitylipoprotein",
						investID: "LAB_ID",
						diseaseID: "LIPIDPANEL",
						name: "(HDL-C) Cholesterol - High-density lipoprotein",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 40,
						maxRangeCI: null,
						unitsCI: "mg/dL",
						minRangeSI: 1.04,
						maxRangeSI: null,
						unitsSI: "mmol/L",
						sequenceId: 3
					},
					{
						id: "(LDLC)CholesterolLowdensitylipoprotein",
						investID: "LAB_ID",
						diseaseID: "LIPIDPANEL",
						name: "(LDL-C) Cholesterol - Low-density lipoprotein",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0,
						maxRangeCI: 130,
						unitsCI: "mg/dL",
						minRangeSI: 0,
						maxRangeSI: 3.36,
						unitsSI: "mmol/L",
						sequenceId: 4
					},
					{
						id: "(TC)TotalCholesterol",
						investID: "LAB_ID",
						diseaseID: "LIPIDPANEL",
						name: "(TC) Total Cholesterol",
						specimen: "Plasma",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 150,
						maxRangeCI: 199,
						unitsCI: "mg/dL",
						minRangeSI: 3.88,
						maxRangeSI: 5.15,
						unitsSI: "mmol/L",
						sequenceId: 1
					},
					{
						id: "Triglycerides(fasting)",
						investID: "LAB_ID",
						diseaseID: "LIPIDPANEL",
						name: "Triglycerides (fasting)",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0,
						maxRangeCI: 250,
						unitsCI: "mg/dL",
						minRangeSI: 0,
						maxRangeSI: 2.82,
						unitsSI: "mmol/L",
						sequenceId: 2
					}
				]
			},
			{
				diseaseName: "Thyroid",
				diseaseID: "THYROID",
				diseaseEntity: [{
						id: "(TSH)Thyroidstimulatinghormone",
						investID: "LAB_ID",
						diseaseID: "THYROID",
						name: "(TSH) Thyroid-stimulating hormone",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0.5,
						maxRangeCI: 5,
						unitsCI: "mcIU/mL",
						minRangeSI: 0.5,
						maxRangeSI: 5,
						unitsSI: "mIU/L",
						sequenceId: 4
					},
					{
						id: "T4Free(Thyroxine)",
						investID: "LAB_ID",
						diseaseID: "THYROID",
						name: "T4: Free (Thyroxine)",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 0.9,
						maxRangeCI: 2.4,
						unitsCI: "ng/dL",
						minRangeSI: 12,
						maxRangeSI: 31,
						unitsSI: "pmol/L",
						sequenceId: 3
					},
					{
						id: "T4Total(Thyroxine)",
						investID: "LAB_ID",
						diseaseID: "THYROID",
						name: "T4: Total (Thyroxine)",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 5,
						maxRangeCI: 12,
						unitsCI: "mcg/dL",
						minRangeSI: 64,
						maxRangeSI: 155,
						unitsSI: "nmol/L",
						sequenceId: 2
					},
					{
						id: "T3Total(Triiodothyronine)",
						investID: "LAB_ID",
						diseaseID: "THYROID",
						name: "T3: Total (Triiodothyronine)",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 70,
						maxRangeCI: 195,
						unitsCI: "ng/dL",
						minRangeSI: 1.1,
						maxRangeSI: 3,
						unitsSI: "nmol/L",
						sequenceId: 1
					}
				]
			},
			{
				diseaseName: "Infection",
				diseaseID: "INFECTION",
				diseaseEntity: [{
						id: "DengueNS1",
						investID: "LAB_ID",
						diseaseID: "INFECTION",
						name: "Dengue NS1",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 1
					},
					{
						id: "DengueIgG",
						investID: "LAB_ID",
						diseaseID: "INFECTION",
						name: "Dengue IgG",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 2
					},
					{
						id: "DengueIgM",
						investID: "LAB_ID",
						diseaseID: "INFECTION",
						name: "Dengue IgM",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 3
					},
					{
						id: "Malaria(Widal)",
						investID: "LAB_ID",
						diseaseID: "INFECTION",
						name: "Malaria (Widal)",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 4
					},
					{
						id: "Typhoid(Widal)",
						investID: "LAB_ID",
						diseaseID: "INFECTION",
						name: "Typhoid (Widal)",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 5
					}
				]
			},
			{
				diseaseName: "Vitamins & Minerals",
				diseaseID: "VITAMINMIN",
				diseaseEntity: [{
						id: "VitaminB12",
						investID: "LAB_ID",
						diseaseID: "VITAMINMIN",
						name: "Vitamin B12",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 200,
						maxRangeCI: 800,
						unitsCI: "pg/mL",
						minRangeSI: 148,
						maxRangeSI: 590,
						unitsSI: "pmol/L",
						sequenceId: 1
					},
					{
						id: "VitaminD31,25Dihydroxycholecalciferol(calcitriol)",
						investID: "LAB_ID",
						diseaseID: "VITAMINMIN",
						name: "Vitamin D3: 1,25-Dihydroxycholecalciferol (calcitriol)",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 25,
						maxRangeCI: 65,
						unitsCI: "pg/mL",
						minRangeSI: 65,
						maxRangeSI: 169,
						unitsSI: "pmol/L",
						sequenceId: 2
					},
					{
						id: "(Ca+)Calcium",
						investID: "LAB_ID",
						diseaseID: "VITAMINMIN",
						name: "(Ca+) Calcium",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 9,
						maxRangeCI: 10.5,
						unitsCI: "mg/dL",
						minRangeSI: 2.2,
						maxRangeSI: 2.6,
						unitsSI: "mmol/L",
						sequenceId: 3
					},
					{
						id: "(PO4)Phosphorus,inorganic",
						investID: "LAB_ID",
						diseaseID: "VITAMINMIN",
						name: "(PO4) Phosphorus, inorganic",
						specimen: "Serum",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 3,
						maxRangeCI: 4.5,
						unitsCI: "mg/dL",
						minRangeSI: 0.97,
						maxRangeSI: 1.45,
						unitsSI: "mmol/L",
						sequenceId: 4
					}
				]
			},
			{
				diseaseName: "HIV Test",
				diseaseID: "HIVTEST",
				diseaseEntity: [{
						id: "HIVElisa",
						investID: "LAB_ID",
						diseaseID: "HIVTEST",
						name: "HIV Elisa",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 1
					},
					{
						id: "HIVRapid",
						investID: "LAB_ID",
						diseaseID: "HIVTEST",
						name: "HIV Rapid",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 2,
						expectedBoolean: false,
						minRangeCI: null,
						maxRangeCI: null,
						unitsCI: 'null',
						minRangeSI: null,
						maxRangeSI: null,
						unitsSI: "null",
						sequenceId: 2
					},
					{
						id: "CD4+Tcellcount",
						investID: "LAB_ID",
						diseaseID: "HIVTEST",
						name: "CD4+ T-cell count",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 640,
						maxRangeCI: 1175,
						unitsCI: "/mcL",
						minRangeSI: 0.64,
						maxRangeSI: 1.18,
						unitsSI:"x 10^9 /L",
						sequenceId: 3
					},
					{
						id: "CD8+Tcellcount",
						investID: "LAB_ID",
						diseaseID: "HIVTEST",
						name: "CD8+ T-cell count",
						specimen: "Blood",
						value: '',
						useCount: 1,
						dataType: 1,
						expectedBoolean: null,
						minRangeCI: 335,
						maxRangeCI: 875,
						unitsCI: "/mcL",
						minRangeSI: 0.34,
						maxRangeSI: 0.88,
						unitsSI: "x 10^9 /L",
						sequenceId: 4
					}
				]
			},
			{
				diseaseName: "Liver",
				diseaseID: "LIVER",
				diseaseEntity: [{
								id:"Albumin",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Albumin",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 3.5,
								maxRangeCI: 5.4,
								unitsCI: "g/dL",
								minRangeSI: 35,
								maxRangeSI: 54,
								unitsSI: "g/L",
								sequenceId: 8
						},
						{
								id:"Alkalinephosphatase(ALP)",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Alkaline phosphatase (ALP)",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 36,
								maxRangeCI: 150,
								unitsCI: "U/L",
								minRangeSI: 0.5,
								maxRangeSI: 2.5,
								unitsSI: "mckat/L",
								sequenceId: 6
						},
						{
								id:"Aminotransferase,alanine(ALT)(SGPT)",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Aminotransferase, alanine (ALT) (SGPT)",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 0,
								maxRangeCI: 35,
								unitsCI: "U/L",
								minRangeSI: 0,
								maxRangeSI: 0.58,
								unitsSI: "pkat/L",
								sequenceId: 5
						},
						{
								id:"Aminotransferase,aspartate(AST)(SGOT)",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Aminotransferase, aspartate (AST) (SGOT)",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 0,
								maxRangeCI: 35,
								unitsCI: "U/L",
								minRangeSI: 0,
								maxRangeSI: 0.58,
								unitsSI: "pkat/L",
								sequenceId: 4
						},
						{
								id:"Bilirubin:In-Direct",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Bilirubin: In-Direct",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 0.3,
								maxRangeCI: 1,
								unitsCI: "mg/dL",
								minRangeSI: 5.1,
								maxRangeSI: 20,
								unitsSI: "mcmol/L ",
								sequenceId: 3
						},
						{
								id:"Bilirubin:Direct",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Bilirubin: Direct",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 0,
								maxRangeCI: 0.3,
								unitsCI: "mg/dL",
								minRangeSI: 0,
								maxRangeSI: 5.1,
								unitsSI: "mcmol/L",
								sequenceId: 2
						},
						{
								id:"Bilirubin:Total",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Bilirubin:Total",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 0.3,
								maxRangeCI: 1.2,
								unitsCI: "mg/dL",
								minRangeSI: 5.1,
								maxRangeSI: 20.5,
								unitsSI: "mcmol/L",
								sequenceId: 1
						},
						{
								id:"Globulins:",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Globulins:",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 2.5,
								maxRangeCI: 3.5,
								unitsCI: "g/dL",
								minRangeSI: 25,
								maxRangeSI: 35,
								unitsSI: "g/L",
								sequenceId: 9
						},
						{
								id:"INR-Therapeuticrange(standardintensitytherapy)",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"INR - Therapeutic range (standard intensity therapy)",
								specimen:"Plasma",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 2,
								maxRangeCI: 3,
								unitsCI: "",
								minRangeSI: null,
								maxRangeSI: null,
								unitsSI: "",
								sequenceId: 10
						},
						{
								id:"Partialthromboplastintime,activated(aPTT)",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Partial thromboplastin time, activated (aPTT)",
								specimen:"Plasma",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 25,
								maxRangeCI: 35,
								unitsCI: "sec",
								minRangeSI: null,
								maxRangeSI: null,
								unitsSI: "",
								sequenceId: 11
						},
						{
								id:"Protein,total",
								investID:"LAB_ID",
								diseaseID: "LIVER",
								name:"Protein, total",
								specimen:"Serum",
								value: null,
								useCount: 1,
								dataType: 1,
								expectedBoolean: null,
								minRangeCI: 6,
								maxRangeCI: 7.8,
								unitsCI: "g/dL",
								minRangeSI: 60,
								maxRangeSI: 78,
								unitsSI: "g/L",
								sequenceId: 7
						}
				]
			}
		]
	},
	{
		investName: "Urine",
        investID:"URINE_ID",
        irDiseaseGP: [
			{
                        diseaseName:"Urine Random",
                        diseaseID:"URINERANDOM",
                        diseaseEntity: [
								{
                                        id: "Microalbumin,albumin/creatinineratio",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Microalbumin, albumin/ creatinine ratio",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 0,
                                        maxRangeCI: 20,
                                        unitsCI:"mcg/mg",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI: "",
										sequenceId: 2,
                                },
                                {
                                        id: "Osmolality",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Osmolality",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 38,
                                        maxRangeCI: 1400,
                                        unitsCI:"mOsm/kg H2O",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 4
                                },
                                {
                                        id: "Phosphate,tubularreabsorption",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Phosphate, tubular reabsorption",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 79,
                                        maxRangeCI: 94,
                                        unitsCI:"% of filtered load",
                                        minRangeSI: null,
                                        maxRangeSI: null,
                                        unitsSI:"",
										sequenceId: 5
                                },
                                {
                                        id: "Porphobilinogens",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Porphobilinogens",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 0,
                                        maxRangeCI: 0.5,
                                        unitsCI:"mg/g creatinine",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 6										
                                },
                                {
                                        id: "pH",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"pH",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 5,
                                        maxRangeCI: 7,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 1
                                },
                                {
                                        id: "Bilirubin",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Bilirubin",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 3
                                },
                                {
                                        id: "Blood",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Blood",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 7										
                                },
                                {
                                        id: "Glucose",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Glucose",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 8										
								}, 
								{
                                        id: "Ketones",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Ketones",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 9										
                                },
                                {
                                        id: "Leukocyteesterase",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Leukocyte esterase",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 10										
                                },
                                {
                                        id: "Nitrites",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Nitrites",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI:"",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 11
                                },
                                {
                                        id: "Protein",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Protein",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 2,
                                        expectedBoolean: false,
                                        minRangeCI: null,
                                        maxRangeCI: null,
                                        unitsCI: "",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 12										
                                },
                                {
                                        id: "Urobilinogen",
                                        investID:"URINE_ID",
                                        diseaseID:"URINERANDOM",
                                        name:"Urobilinogen",
                                        specimen:"Urine",
                                        value: null,
                                        useCount: 1,
                                        dataType: 1,
                                        expectedBoolean: null,
                                        minRangeCI: 0.2,
                                        maxRangeCI: 1,
                                        unitsCI:"EU",
                                        minRangeSI: null,
                                        maxRangeSI: null,
										unitsSI:"",
										sequenceId: 13
                                }
                        ]
            },
            {
                        diseaseName:"Urine 5h",
                        diseaseID:"URINE5",
                        diseaseEntity: [
							{
                                id: "d-Xyloseexcretion5h",
                                investID:"URINE_ID",
                                diseaseID:"URINE5",
                                name:"d-Xylose excretion 5 h after ingestion of 25 g of d-xylose",
                                specimen:"Urine",
                                value: null,
                                useCount: 1,
                                dataType: 1,
                                expectedBoolean: null,
                                minRangeCI: 5,
                                maxRangeCI: 8,
                                unitsCI:"g",
                                minRangeSI: null,
                                maxRangeSI: null,
								unitsSI:"",
								sequenceId: 1
							}
						]
            }
        ]
	}
]


export const RESULTS = new InjectionToken<InvestigationType[]>('RESULTS', {
    factory: () => results
});

// export const results: InvestigationType[] = [{
//     investID: ActiveInvestType.LAB_RESULTS,
//     investName: 'Laboratory',
//     irDiseaseGP: [
//         {
//             diseaseName: 'diabetes',
//             diseaseID: IRDiseaseGPID.DIABETES,
//             diseaseEntity: [
//                 {
//                     name: 'FBSL',
//                     id: 'FBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'HBAIC',
//                     id: 'HBAIC_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'PPBSL',
//                     id: 'PPBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'thyroid',
//             diseaseID: IRDiseaseGPID.THYROID,
//             diseaseEntity: [
//                 {
//                     name: 'TSH',
//                     id: 'TSH_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T3',
//                     id: 'T3_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T4',
//                     id: 'T4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'FREE T4',
//                     id: 'FREET4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'kidney',
//             diseaseID: IRDiseaseGPID.KIDNEY,
//             diseaseEntity: [
//                 {
//                     name: 'NA+',
//                     id: 'NA+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'CREATININE',
//                     id: 'CREATININE_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'K+',
//                     id: 'K+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'BUN',
//                     id: 'BUN_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }
//     ]
// }, {
//     investID: ActiveInvestType.CARDIOLOGY,
//     investName: 'Cardiology',
//     irDiseaseGP: [
//         {
//             diseaseName: 'diabetes',
//             diseaseID: IRDiseaseGPID.DIABETES,
//             diseaseEntity: [
//                 {
//                     name: 'FBSL',
//                     id: 'FBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'HBAIC',
//                     id: 'HBAIC_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'PPBSL',
//                     id: 'PPBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'thyroid',
//             diseaseID: IRDiseaseGPID.THYROID,
//             diseaseEntity: [
//                 {
//                     name: 'TSH',
//                     id: 'TSH_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T3',
//                     id: 'T3_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T4',
//                     id: 'T4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'FREE T4',
//                     id: 'FREET4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'kidney',
//             diseaseID: IRDiseaseGPID.KIDNEY,
//             diseaseEntity: [
//                 {
//                     name: 'NA+',
//                     id: 'NA+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'CREATININE',
//                     id: 'CREATININE_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'K+',
//                     id: 'K+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'BUN',
//                     id: 'BUN_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }
//     ]
// },  {
//     investID: ActiveInvestType.CHEST_RES,
//     investName: 'Chest',
//     irDiseaseGP: [
//         {
//             diseaseName: 'diabetes',
//             diseaseID: IRDiseaseGPID.DIABETES,
//             diseaseEntity: [
//                 {
//                     name: 'FBSL',
//                     id: 'FBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'HBAIC',
//                     id: 'HBAIC_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'PPBSL',
//                     id: 'PPBSL_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.DIABETES,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'thyroid',
//             diseaseID: IRDiseaseGPID.THYROID,
//             diseaseEntity: [
//                 {
//                     name: 'TSH',
//                     id: 'TSH_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T3',
//                     id: 'T3_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'T4',
//                     id: 'T4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'FREE T4',
//                     id: 'FREET4_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.THYROID,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }, {
//             diseaseName: 'kidney',
//             diseaseID: IRDiseaseGPID.KIDNEY,
//             diseaseEntity: [
//                 {
//                     name: 'NA+',
//                     id: 'NA+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'CREATININE',
//                     id: 'CREATININE_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'K+',
//                     id: 'K+_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }, {
//                     name: 'BUN',
//                     id: 'BUN_ID',
//                     value: '',
//                     unit: 'UI',
//                     diseaseID: IRDiseaseGPID.KIDNEY,
//                     investID: ActiveInvestType.LAB_RESULTS
//                 }
//             ]
//         }
//     ]
// }];

