
export class Address {
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    pincode: string;
    country: string;
    description: string;
    latitude: string;
    longitude: string;
    fullAddress: string;
    
    get address() {
        return this.fullAddress;
    }
}
