import { InjectionToken } from "@angular/core";

export type LAZY_MODULES = {
    add_patient_to_queue: string;
    lab_form_rendrr: string;
};

export const lazyMap: LAZY_MODULES = {
    add_patient_to_queue: 'src/app/feature-modules/queue/add-patient-to-queue/add-patient-to-queue.module#AddPatientToQueueModule',
    lab_form_rendrr: 'src/app/feature-modules/laboratory/lab-form-rendrr/lab-form-rendrr.module#LabFormRendrrModule'
};

export const LAZY_MODULES_MAP = new InjectionToken('LAZY_MODULES_MAP', {
    factory: () => lazyMap
});
