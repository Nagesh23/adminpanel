import { Type } from "class-transformer";
import { BaseResponse } from "./base_response";

export class SOSFrequenciesResponse extends BaseResponse {
    @Type(() => SOSFrequency)
    data: SOSFrequency[];
}

export class SOSFrequency {
    created: number;
    updated: number;
    version: number;
    title: string;
    score: number;
    sosId: string;
}
