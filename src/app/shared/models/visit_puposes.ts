import { BaseResponse } from './base_response';
import { Type } from "class-transformer";

export class VisitPurposesResponse extends BaseResponse {
    @Type(() => VisitPurpose)
    data: VisitPurpose[];
}
export class VisitPurpose {
    purpose: string;
    code: number;
    visitPurposeId: string;
    canMultiple: boolean;
}

