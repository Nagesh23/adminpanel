import { Type } from "class-transformer";
import { StockMedicine } from './stock_medicine';
import { BaseResponse } from './base_response';

export class SearchMedicineResponse extends BaseResponse {
    @Type(() => SearchMedicine)
    data: SearchMedicine[];
}
export class SearchMedicine {
    name: string;
    form: string;
    standardUnits: number;
    packageForm: string;
    price: number;
    size: string;
    manufacturer: string;
    @Type(() => Constituent)
    constituents: Constituent[];
    @Type(() => Schedule)
    schedule: Schedule;
    id: string;
    medicine_id: string;
    search_score: number;
    usedCount = 0;
    brandCount = 0;
    @Type(() => StockMedicine)
    stockMedicines: StockMedicine[];
    isActive: boolean;
}

export class Constituent {
    name: string;
    strength: string;
}

export class Schedule {
    category: string;
    label: string;
}


