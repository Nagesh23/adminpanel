import { BaseResponse } from './base_response';

export class OnlineUsersResponseModel extends BaseResponse {    
    data: OnlineUsersData;
}

export class OnlineUsersData {
    onlineUsersCount: number;
    loginUsersList: LoginUsersData[];
}

export interface LoginUsersData {
    userId: string;
    userName: string;
    phoneNumber: string;
    city: string;
    role: string;
    clinic: string;
    loginTime: string;
    lastInteractionTime: string;
    source: string;
}
