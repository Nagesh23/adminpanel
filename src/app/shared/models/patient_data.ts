import { Address } from "./address";
import { Type } from "class-transformer";
import { BaseResponse } from './base_response';
import { Refer2Phy } from './refer2_phy';

export class SearchPatients extends BaseResponse {
    @Type(() => Patient)
    data: Patient[];
}

export class PatientRespnse extends BaseResponse {
    @Type(() => Patient)
    data: Patient;
}

export class Patient {
    dob: string;
    gender: string;
    emailId: string;
    phoneNumber: string;
    imageUrl: string;
    upId: string;
    creatorId: string;
    patientId: string;
    age: string;
    @Type(() => AssociatedDependent)
    associatedDependents: AssociatedDependent[];
    @Type(() => Address)
    address: Address;
    patientType: number;
    creatorType: number;
    ageInYear: number;
    dobInMilli: number;
    salutation: string;
    serialNumber: number;
    fileCode: string;
    clinicId: string;
    dependent: boolean;
    firstName: string;
    lastName: string;
    patientOtpVerification: boolean;
    referencePhysicianData: Refer2Phy;
    constructor(phoneNumber: string, address: Address) {
        this.phoneNumber = phoneNumber;
        this.address = address;
    }
}

export class AssociatedDependent {
    creatorId: string;
    @Type(() => Patient)
    dependent: Patient;
    relationSheep: string;
}
