import { Type } from "class-transformer";

export class TranslatedTextModel {
    @Type(() => Translations)
    data: Translations;
}
export class Translations {
    @Type(() => TranslatedText)
    translations: TranslatedText[];
}
export class TranslatedText {
    translatedText: string;
}