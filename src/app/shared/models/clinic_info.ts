import { Address } from "./address";
import { Type } from 'class-transformer';
import { BaseResponse } from "./base_response";

export class ClinicTimingList {
    timingFrom: string;
    timingTo: string;
    days: number[] = [];
    weekFrequencies: number[] = [];
    
    // custom fields and function
    hoursFrom: string;
    minutesFrom: string;
    hoursTo: string;
    minutesTo: string;
    isActive: object = [];
    isValid = false;

    get hoursFromValue() {
        return this.timingFrom ? this.timingFrom.trim().split(':' || '/' || '-')[0] : '';
    }
    get minutesFromValue() {
        return this.timingFrom ? this.timingFrom.trim().split(':' || '/' || '-')[1] : '';
    }
    get hoursToValue() {
        return this.timingFrom ? this.timingTo.trim().split(':' || '/' || '-')[0] : '';
    }
    get minutesToValue() {
        return this.timingTo ? this.timingFrom.trim().split(':' || '/' || '-')[1] : '';
    }
}
export class ClinicModel extends BaseResponse {
    @Type(() => ClinicInfoList)
    data: ClinicInfoList[];
}

export class ClinicInfoList {
    clinicId: string;
    @Type(() => ClinicTimingList)
    clinicTimingList: ClinicTimingList[] = [];
    role: number;
    hideLetterHead: boolean;
    physiciansClinicId: string;
    logo: string;
    name: string;
    @Type(() => Address)
    clinicAddress: Address;
    pharmacyId: string;
    companyId: string;
    appointmentNumbers: string[] = [];
    services: string[] = [];
    code: string;
    minimumConsultaionDuration: number;
    codeDate: string;
    patientOtpVerification: boolean;
    // custom fields 
    filteredClinics: ClinicInfoList[] = [];
    fullAddress: string;
    templateName: string;
}
