export enum StorageKeys {
    // physicians storage keys
    AUTH_TOKEN = 'authToken',
    LOGGED_IN_PHYSICIAN = 'physician',
    VITALS_ENTITIES = 'vital_entities_value',
    PRACTICING_CLINIC = 'physician_clinic',
    PATIENTS_IN_Q = 'patients_in_queue',
    PATIENT_SUMMARY = 'patient_summary',
    CURRENT_PATIENT = 'current_patient',
    PHYSICIANs_PRESCN_TEMPLATES = 'physician_templates',
    RQD_SNOMED_DATA = 'snomed_data',
    FREQN8LY_USED_SNOMED_DATA = 'freqn8ly_used_snomed_data',
    PHY_DEFAULT_MEDICINES = 'phy_default_medicines',
    ALL_DOSE_TYPES = 'dose_types',
    MEDICINE_FREQUENCIES = 'medicine_frequencies',
    FREQUENTLY_USED_COMMENTS = 'frequently_used_comments',
    CURRENT_PATIENT_PRESCRIPTION = 'current_patient_prescription',
    LANGUAGE_KEYWORDS = 'translated_keywords',

    // admin users storage keys
    LOGGED_IN_ADMIN_USER = 'adminUser',
    APPOINTMENT_LIST = 'appointment_list'
}