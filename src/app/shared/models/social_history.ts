import { BaseResponse } from "./base_response";
import { Type } from "class-transformer";
import { SnomedSearch } from './snomed_search';

export class SocialHistory {
    created: number;
    updated: number;
    version: number;
    patientId: string;
    physicianId: string;
    clinicId: string;
    frequencyType: string;
    title: string;
    titleSnomedId: string;
    @Type(() => SnomedSearch)
    descriptionList: SnomedSearch[];
    patientSocialHistoryId: string;
}

export class SocialHistoryResponse extends BaseResponse {
    @Type(() => SocialHistory)
    data: SocialHistory[];
}
