import { BaseResponse } from './base_response';
import { Type } from "class-transformer";
import { AdminUserData } from './adminuser_data';

export class LoginResponseModel extends BaseResponse {
    // @Type(() => LoginResponse)
    data: LoginResponse;
}

export class LoginResponse {
    token: string;
    // @Type(() => AdminUserData)
    adminUserData: AdminUserData;
}
