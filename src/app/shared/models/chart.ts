import { BaseResponse } from './base_response';
import { Type } from "class-transformer";

export class ChartResponse extends BaseResponse {
    @Type(() => Chart)
    data: Chart;

}
export class Chart {
    id: string;
    gender: number;
    vitalId: string;
    Age: number;
    L: number;
    M: number;
    S: number;
    P01?: any;
    P1?: any;
    P3: number;
    P5: number;
    P10: number;
    P15?: any;
    P25: number;
    P55?: any;
    P75: number;
    P85?: any;
    P90: number;
    P95: number;
    P97: number;
    P99?: any;
    P999?: any;
}
