import { Type } from "class-transformer";
import { TimeSlot } from "./appointment_data";
import { BaseResponse } from "./base_response";
import { ClinicData, PhysicianData } from "./physician_data";

export class DateWiseSlotsRes extends BaseResponse {
    @Type(() => DateWiseSlots)
    data: DateWiseSlots[];
}

/**
 * This is date wise slots
 */

export class DateWiseSlots {
    date: string;
    formatedDate: string;
    /**
     * this is clinic slots info.
     */
    clinicSlots: ClinicSlot[];
    physicianSlots: PhysicianSlot[];
}
/**
 * this is clinic slots info.
 */

export class ClinicSlot {
    clinicData: ClinicData;
    /**
     * this is slots related to clinic.
     */
    timeSlots: TimeSlot[];
    mrSlots: TimeSlot[];
}

export class PhysicianSlot {
    physicianData: PhysicianData;
    timeSlots: TimeSlot[];
    mrSlots: TimeSlot[];
}
