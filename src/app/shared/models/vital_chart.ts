import { BaseResponse } from './base_response';
import { Type } from "class-transformer";

export class VitalChartResponse extends BaseResponse {
    @Type(() => VitalChart)
    data: VitalChart[];
}
export class VitalChart {
    xValue: string;
    bpDiastolic: number;
    bpSystolic: number;
    @Type(() => OtherList)
    otherList: OtherList[];
}

export class OtherList {
    vitalId: string;
    value: number;
}

export class VitalDataProvider {
    open: number;
    close: number;
    xValue: string;
}
export class VitalGraphData { }
