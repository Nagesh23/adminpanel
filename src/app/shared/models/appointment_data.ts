import { Patient } from "./patient_data";
import { PhysicianData } from "./physician_data";
import { ClinicInfoList } from "./clinic_info";
import { BaseResponse } from "./base_response";
import { Type } from "class-transformer";

export class AppointmentsResponse extends BaseResponse {
    @Type(() => AppointmentData)
    data: AppointmentData[];
    totalCount: number;
}

export class AppointmentData {
    created: number;
    physicianId: string;
    clinicId: string;
    patientId: string;
    date: string;
    timeSlot: TimeSlot;
    /**
     * @enum
     * @example 
     * AppointmentType
     * 1 for IN_CLINIC APPOINTMENT
     * 2 FOR REMOTE APPOINTMENT
     */
    appointmentType = 1;
    /**
     * @enum
     * @example
     *  AppointmentStatus
     *  1 FOR INITIATED
     *  2 FOR ACCEPTED
     *  3 FOR VISIT_CLINIC
     *  4 BOOKED
     *  5 CANCELED
     *  6 EXPIRED
     *  7 RE_SCHEDULE
     *  8 COMPLETED
     *  9 VISIT_EMERGENCY
     *  10 CLINIC_VISITED
     */
    appointmentStatus: number;
    appointmentTime: number;
    consultationType: number = null;
    description: string;
    patientData: Patient;
    physicianData: PhysicianData;
    assistantPhysicianData: PhysicianData;
    appointmentId: string;
    appointmentStatusData: AppointmentStatusData;
    amount: number;
    refundData: RefundData;
    // custom fields
    loader: boolean;
    displayDate: string | number;
    clinicData: ClinicInfoList;
    openBkSlots: boolean;
    makeCall: boolean;
    appointmentStatusName: string;
}

export class TimeSlot {
    startTime: string;
    endTime: string;
    startDateTime: Date;
    time: number;
}

export interface AppointmentStatusData {
    created: number;
    updated: number;
    userId: string;
    userRole: number;
}

export class RefundData {
    created: number;
    updated: number;
    version: number;
    refundId: number;
    referenceId: number;
    txAmount: number;
    refundAmount: number;
    orderId: string;
    note: string;
    processed: string;
    initiatedOn: string;
    processedOn: string
    cashfreeRefundId: string
}
