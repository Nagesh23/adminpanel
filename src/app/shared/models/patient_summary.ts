import { Type } from 'class-transformer';
import { BaseResponse } from './base_response';
import { Prescription } from './prescription';
import { SnomedSearch } from './snomed_search';
import { Medicine, RecommendedMedicineResponse } from './medicine';
import { VitalChart } from './vital_chart';
import { Chart } from "./chart";
import { Vital } from './vitals';
export class PatientSummaryResponse extends BaseResponse {
    @Type(() => PatientSummary)
    data: PatientSummary;
}

export class PatientSummary {
    @Type(() => PrescriptionSummary)
    prescriptionSummary: PrescriptionSummary;
    @Type(() => VitalChart)
    vitalChartList: VitalChart[];
    @Type(() => Prescription)
    pastPrescriptions: Prescription[];
    @Type(() => Chart)
    grownthChartEntries: Chart[];
    @Type(() => RecommendedMedicineResponse)
    recommendedMedications: RecommendedMedicineResponse[];
    @Type(() => Vital)
    toDaysVitals: Vital[];
}

export class PrescriptionSummaryResponse extends BaseResponse {
    @Type(() => PrescriptionSummary)
    data: PrescriptionSummary;
}

export class PrescriptionSummary {
    @Type(() => Prescription)
    lastPrescription: Prescription;
    @Type(() => CurrentMedicines)
    currentMedicines: CurrentMedicines;
    @Type(() => DiagnosisDataList)
    diagnosisDataList: DiagnosisDataList[];
    @Type(() => DoctorsNotesList)
    doctorsNotesList: DoctorsNotesList[];
}
    // controls: {
    //     [key: string]: AbstractControl;
    // };
export class CurrentMedicines {
    [key: string]: CurrentMedicinesName;
}

export class CurrentMedicinesName {
    endDate: string;
    @Type(() => Medicine)
    medication: Medicine;
    precriptionId: string;
}

export class DiagnosisDataList {
    date: string;
    @Type(() => SnomedSearch)
    diagnosisList: SnomedSearch[];
}

export class DoctorsNotesList {
    date: string;
    doctoresNote: string;
}
