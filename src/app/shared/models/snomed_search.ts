import { Type } from 'class-transformer';
import { BaseResponse } from './base_response';

export class DiagnosisSearchResponse extends BaseResponse {
    @Type(() => DiagnosisSearch)
    data: DiagnosisSearch[];
}

export class DiagnosisSearch {
    tags: string;
    @Type(() => SnomedSearch)
    diagnosisList: SnomedSearch;
}

export class SnomedSearchResponse extends BaseResponse {
    @Type(() => SnomedSearch)
    data: SnomedSearch[];
    type: SnomedType;
}

export class SnomedSearch {
    name: string;
    snomedId: string;
    hierachy: string;
    usedCount: number;
    /** custom fields */ 
    checked: boolean;
    constructor(name: string) {
        this.name = name;
    }
}

export enum SnomedType {
    CHIEF_COMPLAINT = 1,
    DIAGNOSIS = 2,
    ALLERGIES = 3, 
    PAST_CURRENT_ILLNESS = 4,
    SYSTEM_REVIEWS = 5,
    INVESTIGATIONS = 6,
    MEDICINES = 7,
}

// export class RQRSnomedData2RITPrescn {
//     [key: number]: SnomedSearch[];
// }