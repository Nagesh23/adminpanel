
export class MyAppointments {
    physicianId: string;
    clinicId: string;
    patientName: string;
    appointmentStatusList: any[];
    clinicIdList: any[];
    physicianIdList: any[];
    fromDate: string;
    toDate: string;
    offset: number;
    limit: number;
}
