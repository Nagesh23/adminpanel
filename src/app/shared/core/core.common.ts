import { CommonService } from "./services/common.service";
import { StringForbiddenDirective } from "./directives/string_forbidden.directive";
import { HttpRequestService } from "./services/http-request.service";
import { RoutingService } from "./services/routing.service";
import { AuthLoginGuard } from "./services/auth-login.guard";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { HeaderInterceptorService } from "./services/http-interceptors.service";
import { OverlayService } from "./services/overlay.service";
import { RunAlertService } from "../services/run-alert.service";
import { StorageService } from './services/storage.service';
import { IonRoutingService } from './services/ionic-routing.service';
// import { PhyOnLoadService } from "./services/phy-on-load.service";
import { ModuleLoaderDirective } from "./directives/module_loader.directive";
import { AscendingSortPipe } from './directives/ascending-sort.pipe';
// import { PreloadSelectedModulesListService } from "./services/preloading.service";
import { LangTranslator } from './directives/lang-translator.pipe';
import { HtmlTagsKillerDirective } from './directives/html-tags-killer.directive';
import { MessagingService } from './services/messaging.service';

export const componentDeclarations: any[] = [
    StringForbiddenDirective, ModuleLoaderDirective, AscendingSortPipe, LangTranslator, HtmlTagsKillerDirective
];

// PhyOnLoadService, PreloadSelectedModulesListService
export const providerDeclarations: any[] = [ CommonService, HttpRequestService, RoutingService, AuthLoginGuard, OverlayService,
    RunAlertService, IonRoutingService ,StorageService, MessagingService,
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptorService, multi: true } 
];

export const exportsFiles: any[] = [ StringForbiddenDirective, ModuleLoaderDirective,
AscendingSortPipe, LangTranslator, HtmlTagsKillerDirective ];
