import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[htmlTagsKiller]'
})
export class HtmlTagsKillerDirective {
    
    @Input('htmlTagsKiller') set hmlTagsKiller(valueWithTags: string) {
        if (valueWithTags) {
            var regex = /(&nbsp;|&emsp;|&ensp;|<([^>]+)>)/ig;
            // /<(?:.|\n)*?>/gm
            const tagsFreeValue = valueWithTags.replace(regex, '');
            if (tagsFreeValue && tagsFreeValue.trim()) {
                this._contRef.createEmbeddedView(this._tempRef);
            } else { this._contRef.clear(); }
        } else { this._contRef.clear(); }
    }
    constructor(private _tempRef: TemplateRef<any>, private _contRef: ViewContainerRef) { }
}
