import { Directive } from '@angular/core';

import { Validator, FormControl, AbstractControl, NG_VALIDATORS } from '@angular/forms';

export function stringForbiddenValidator() {
    return (c: AbstractControl): {[key: string]: any} => {
        if (c.value) {
            const isNan = isNaN(c.value);
            if (isNan) {
                return { 'string_forbidden': { valid: false }};
            } else {
                return null;
            }
        }
    };
}

@Directive({ 
    selector: '[string_forbidden][ngModel]',
    providers: [{
        provide: NG_VALIDATORS, useExisting: StringForbiddenDirective, multi: true
    }]
})

export class StringForbiddenDirective implements Validator {
    constructor() { }
    validate(c: FormControl) {
        return stringForbiddenValidator()(c);
    }
}
