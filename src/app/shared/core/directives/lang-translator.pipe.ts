import { Pipe, PipeTransform } from '@angular/core';
import { TranslatedTextModel } from '../../models/translated_text';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Pipe({
    name: 'translate'
})

export class LangTranslator implements PipeTransform {
    constructor(private _http: HttpClient) {}
    transform(queryToTrans: string, baseLang: string, targetLang: string) {
        if (queryToTrans && (targetLang !== baseLang)) {
            const tagsFreeValue = queryToTrans.replace(/<(?:.|\n)*?>/gm, '');
            const url: string = 'https://translation.googleapis.com/language/translate/v2/?key='
                + 'AIzaSyAtY0ZfnU5nl4FhmAjIn-nS7pUXwlot9IA' + '&q=' + tagsFreeValue + '&source=' + baseLang + '&target=' + targetLang;
            return this._http.get<TranslatedTextModel>(url).pipe(
                map((res: TranslatedTextModel) => {
                return res.data ? res.data.translations[0].translatedText : queryToTrans;
            }));
        } else { return of(queryToTrans ? queryToTrans : ''); }
    }
}
