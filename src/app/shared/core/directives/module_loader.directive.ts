import { OnInit, Input, NgModuleRef, ViewContainerRef, Injector, NgModuleFactoryLoader, Inject, 
    NgModuleFactory, OnDestroy, Directive } from '@angular/core';
import { LAZY_MODULES_MAP, LAZY_MODULES } from '../../models/lazy-modules';

// tslint:disable-next-line:directive-selector
@Directive({ selector: '[loadModule]'})

export class ModuleLoaderDirective implements OnInit, OnDestroy {

    @Input('loadModule') moduleName: keyof LAZY_MODULES;
    moduleRef: NgModuleRef<any>;
    constructor(private _viewContRef: ViewContainerRef, private _injector: Injector,
        private _loader: NgModuleFactoryLoader, @Inject(LAZY_MODULES_MAP) private modulesMap) { 
            console.log('constructor of module loader');
        }
        
    ngOnInit() {
        console.log('module loader directive');
        this._loader.load(this.modulesMap[this.moduleName]).
        then((moduleFactory: NgModuleFactory<any>) => {
            this.moduleRef = moduleFactory.create(this._injector);
            const rootComponent = (moduleFactory.moduleType as any).rootComponent;
            const factory = this.moduleRef.componentFactoryResolver.resolveComponentFactory(rootComponent);
            this._viewContRef.createComponent(factory);
        });
    }
    ngOnDestroy() {
        this.moduleRef && this.moduleRef.destroy();
    }
}
