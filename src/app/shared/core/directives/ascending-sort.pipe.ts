import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ascending_sort'
})

export class AscendingSortPipe implements PipeTransform {

    transform(value: any[], prop: string): any {
        const result = value.sort((a, b) => {return a[prop] - b[prop]});
        return result;
    }
}