import { NgModule, ModuleWithProviders } from '@angular/core';
import { providerDeclarations, componentDeclarations, exportsFiles } from './core.common';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../widgets_module/material/material.module';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [...componentDeclarations],
    imports: [ CommonModule, HttpClientModule, MaterialModule ],
    exports: [...exportsFiles]
})

export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [...providerDeclarations]
        }
    }
}
