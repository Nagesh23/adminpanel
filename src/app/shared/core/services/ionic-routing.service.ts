import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UrlTree } from '@angular/router';

@Injectable()
export class IonRoutingService {

    constructor(private _navCont: NavController) { }

    navigateForward(url: string | UrlTree | any[], options?: any) {
        this._navCont.navigateForward(url, options);
    }
    navigateBack(url: string | UrlTree | any[], options?: any) {
        this._navCont.navigateBack(url, options);
    }
    navigateRoot(url: string | UrlTree | any[], options?: any) {
        this._navCont.navigateRoot(url, options);
    }
}