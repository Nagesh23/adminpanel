import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,
    CanLoad, UrlSegment, CanActivateChild, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RoutingService } from './routing.service';
import { CommonService } from './common.service';

@Injectable()
export class AuthLoginGuard implements CanActivate, CanLoad, CanActivateChild {
    constructor(private _comSer: CommonService, private router: RoutingService) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            // console.log('can activate');
            return this.isLogin();
    }
    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            // console.log('can activate child');
            return this.isLogin();
    }
    isLogin(): boolean {
        const authToken = this._comSer.authToken;
        if (authToken) {
            return true;
        } else {
            this._comSer.runAlert({ status: true, title: 'Logged Out', description: 'Please Login' });
            this.router.navigate(['/login']);
            return false;
        }
    }
    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        // console.log('can load');
        return this.isLogin();
    }
}

export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}
