// import { Injectable } from '@angular/core';
// import { HttpRequestService } from './http-request.service';
// import { Vital, VitalResponse } from '../../models/vitals';
// import { take, map, filter } from 'rxjs/operators';
// import { StorageService } from './storage.service';
// import { CommonService } from './common.service';
// import { HttpURLS } from '../../models/api_urls';
// import { StorageKeys } from '../../models/storage_keys';
// import { Prescription, PrescriptionResponse } from '../../models/prescription';
// import { SnomedSearchResponse, SnomedType, SnomedSearch } from '../../models/snomed_search';
// import { merge } from 'rxjs';
// import { SearchMedicine, SearchMedicineResponse } from '../../models/search_medicine';
// import { BaseResponse, Response } from '../../models/base_response';
// import { SOSFrequenciesResponse, SOSFrequency } from "../../models/frequencies";
// import { CommentsResponse, Comment } from "../../models/comments";
// import { LanguageKeyword, LanguageKeywordModel } from "../../models/language_keyword";

// @Injectable()
// export class AdminUserOnLoadService {
//     private _vitals: Vital[];
//     private _prescnTemplates: Prescription[];
//     constructor(private _httpReq: HttpRequestService, private _comSer: CommonService,
//         private _storage: StorageService) { }
//     private _reqSMData2RITPrescn: Map<SnomedType, SnomedSearch[]>;
//     private _freqn8lyUsedSnomed: Map<SnomedType, SnomedSearch[]>;
//     private _defaultMedOfPhy: SearchMedicine[];
//     private _doseTypes: string[];
//     private _frequencies: SOSFrequency[];
//     private _frequentlyUsedComments: Comment[];
//     private _languageKeywords: LanguageKeyword[]; 
//     async getVitalEntities(): Promise<Vital[]> {
//         if (!this._vitals) {
//             this._vitals = await this._storage.getItem<VitalResponse>(StorageKeys.VITALS_ENTITIES, VitalResponse)
//                 .then((res: VitalResponse) => (!res.error && res.data) ? res.data : []);
//         }
//         return this._vitals;
//     }
//     getVitalEntitiesFromServer() {
//         this._comSer.showLoader();
//         this._httpReq.getResponse<VitalResponse>(VitalResponse, HttpURLS.ALL_VITALS).pipe(take(1)).
//             subscribe((res: VitalResponse) => {
//                 this._vitals = (!res.error && res.data) ? res.data : [];
//                 this._storage.setItem<VitalResponse>(StorageKeys.VITALS_ENTITIES, res);
//             });
//     }
//     async getPrescnTemplates() {
//         if (!this._prescnTemplates) {
//             this._prescnTemplates = await this._storage.getItem<PrescriptionResponse>(StorageKeys.PHYSICIANs_PRESCN_TEMPLATES,
//                 PrescriptionResponse).then((res: PrescriptionResponse) => (!res.error && res.data) ? res.data : []);
//         }
//         return this._prescnTemplates;
//     }
//     getPrescriptonTemplateFromServer() {
//         this._comSer.showLoader();
//         this._httpReq.getResponse<PrescriptionResponse>(PrescriptionResponse,
//             HttpURLS.PHYSICIANs_PRESCN_TEMPLATE + this._comSer.physician.physicianId).
//             subscribe((res: PrescriptionResponse) => {
//                 this._prescnTemplates = (!res.error && res.data) ? res.data : [];
//                 this._storage.setItem<PrescriptionResponse>(StorageKeys.PHYSICIANs_PRESCN_TEMPLATES, res);
//             });
//     }

//     /** SNOMED DATA BY THEIR SNOMED TYPE */

//     async getREQSMData2RITPrescn(): Promise<Map<SnomedType, SnomedSearch[]>> {
//         if (!this._reqSMData2RITPrescn) {
//             this._reqSMData2RITPrescn = await this._storage.getItem<Map<SnomedType, SnomedSearch[]>>(StorageKeys.RQD_SNOMED_DATA, Map);
//         }
//         return this._reqSMData2RITPrescn;
//     }
//     async getSnomedDataByType(snomedType: SnomedType): Promise<SnomedSearch[]> {
//         const snomedData: Map<SnomedType, SnomedSearch[]> = await this.getREQSMData2RITPrescn();
//         return snomedData.get(snomedType);
//     }
//     setSnomedData() {
//         this._reqSMData2RITPrescn = new Map<SnomedType, SnomedSearch[]>();
//         merge(this._getSnomedDataByType(SnomedType.CHIEF_COMPLAINT), this._getSnomedDataByType(SnomedType.DIAGNOSIS),
//         this._getSnomedDataByType(SnomedType.ALLERGIES), this._getSnomedDataByType(SnomedType.INVESTIGATIONS))
//         .subscribe((res: SnomedSearchResponse) => {
//             this._reqSMData2RITPrescn.set(res.type, ((!res.error && res.data) ? res.data : []));
//             this._storage.setItem(StorageKeys.RQD_SNOMED_DATA, this._reqSMData2RITPrescn);
//         });
//     }
//     private _getSnomedDataByType(type: SnomedType) {
//         this._comSer.showLoader();
//         const url = HttpURLS.SNOMED_DATA_BY_TYPE + type + '&physicianId=' + this._comSer.physician.physicianId;
//         return this._httpReq.getResponse<SnomedSearchResponse>(SnomedSearchResponse, url).pipe(
//             map((res: SnomedSearchResponse) => (res.type = type, res))
//         );
//     }

//     /** FREQUENTLY USED SNOMED DATA BY THEIR SNOMED TYPE */

//     setFreqn8lyUsedSnomedData() {
//         this._freqn8lyUsedSnomed = new Map<SnomedType, SnomedSearch[]>();
//         merge(this._getFreqn8lyUsedSnomedDataByTypeFromServer(SnomedType.DIAGNOSIS))
//             .subscribe((res: SnomedSearchResponse) => {
//                 this._freqn8lyUsedSnomed.set(res.type, ((!res.error && res.data) ? res.data : []));
//                 this._storage.setItem(StorageKeys.FREQN8LY_USED_SNOMED_DATA, this._freqn8lyUsedSnomed);
//             });
//     }
//     private _getFreqn8lyUsedSnomedDataByTypeFromServer(type: SnomedType) {
//         this._comSer.showLoader();
//         const url = HttpURLS.FRQN8LY_SNOMED_DATA_BY_TYPE + type + '&physicianId=' + this._comSer.physician.physicianId;
//         return this._httpReq.getResponse<SnomedSearchResponse>(SnomedSearchResponse, url).pipe(
//             map((res: SnomedSearchResponse) => (res.type = type, res))
//         );
//     }
//     async getFreqn8lyUsedSnomed(): Promise<Map<SnomedType, SnomedSearch[]>> {
//         if (!this._freqn8lyUsedSnomed) {
//             this._freqn8lyUsedSnomed = await this._storage.
//             getItem<Map<SnomedType, SnomedSearch[]>>(StorageKeys.FREQN8LY_USED_SNOMED_DATA, Map);
//         }
//         return this._freqn8lyUsedSnomed;
//     }
//     async getFreqn8lyUsedSnomedDataByType(snomedType: SnomedType): Promise<SnomedSearch[]> {
//         const snomedData: Map<SnomedType, SnomedSearch[]> = await this.getFreqn8lyUsedSnomed();
//         return snomedData.get(snomedType);
//     }

//     /** Set default medicines of physician  */

//     setDefaultMedicineOfPhy() {
//         const url = HttpURLS.PHY_DEFAULT_MEDICINES + this._comSer.physician.physicianId;
//         this._httpReq.getResponse<SearchMedicineResponse>(SearchMedicineResponse, url)
//         .subscribe((res: SearchMedicineResponse) => {
//             this._defaultMedOfPhy = !res.error && res.data ? res.data : [];
//             this._storage.setItem(StorageKeys.PHY_DEFAULT_MEDICINES, res);
//         });
//     }
//     async getDefaultMedicineOfPhy(): Promise<SearchMedicine[]> {
//         if (!this._defaultMedOfPhy) {
//             this._defaultMedOfPhy = await this._storage.
//             getItem<SearchMedicineResponse>(StorageKeys.PHY_DEFAULT_MEDICINES,
//             SearchMedicineResponse).then((res: SearchMedicineResponse) => {
//                 return !res.error && res.data ? res.data : [];
//             });
//         }
//         return this._defaultMedOfPhy;
//     }

//     /** Set List of dose types */

//     getDoseTypesFromServer() {
//         this._httpReq.getResponse<Response>(Response, HttpURLS.ALL_DOSE_TYPES).pipe(
//             map((res: Response) => {
//                 this._doseTypes = !res.error && res.data ? res.data : [];
//                 this._storage.setItem(StorageKeys.ALL_DOSE_TYPES, res);
//             })
//         ).subscribe();
//     }
//     async getDoseTypes(): Promise<string[]> {
//         if (!this._doseTypes) {
//             this._doseTypes = await this._storage.getItem<Response>(StorageKeys.ALL_DOSE_TYPES, Response).then(
//                 (res: Response) =>  { return !res.error && res.data ? res.data : []; }
//             );
//         }
//         return this._doseTypes;
//     }

//     /** Set List of medicine frequency */
//     getSOSFrequenciesFromServer() {
//         this._httpReq.getResponse<SOSFrequenciesResponse>(Response, HttpURLS.MEDICINE_FREQUENCIES).pipe(
//             map((res: SOSFrequenciesResponse) => {
//                 this._frequencies = !res.error && res.data ? res.data : [];
//                 this._storage.setItem(StorageKeys.MEDICINE_FREQUENCIES, res);
//             })
//         ).subscribe();
//     }
//     async getSOSFrequencies(): Promise<SOSFrequency[]> {
//         if (!this._frequencies) {
//             this._frequencies = await this._storage.
//             getItem<SOSFrequenciesResponse>(StorageKeys.MEDICINE_FREQUENCIES, SOSFrequenciesResponse).then(
//                 (res: SOSFrequenciesResponse) =>  { return !res.error && res.data ? res.data : []; }
//             );
//         }
//         return this._frequencies;
//     }

//     /** Set Comments list of physician as per the selected clinic */

//     getPhyFrequentlyUsedCommentsFromServer() {
//         const url = HttpURLS.FREQUENTLY_USED_COMMENTS + this._comSer.physician.physicianId + '&clinicId=' + this._comSer.clinic.clinicId;
//         this._httpReq.getResponse<CommentsResponse>(CommentsResponse, url).pipe(
//             map((res: CommentsResponse) => {
//                 this._frequentlyUsedComments = !res.error && res.data ? res.data : [];
//                 this._storage.setItem(StorageKeys.FREQUENTLY_USED_COMMENTS, res);
//             })
//         ).subscribe();
//     }
//     async getFrequentlyUsedComments(): Promise<Comment[]> {
//         if (!this._frequentlyUsedComments) {
//             this._frequentlyUsedComments = await this._storage.
//             getItem<CommentsResponse>(StorageKeys.FREQUENTLY_USED_COMMENTS, CommentsResponse).then(
//                 (res: CommentsResponse) =>  { return !res.error && res.data ? res.data : []; }
//             );
//         }
//         return this._frequentlyUsedComments;
//     }

//     /** Set Language keywords for translation */

//     getLanguageKeywordsFromServer() {
//         const url = HttpURLS.LANGUAGE_KEYWORDS;
//         this._httpReq.getResponse<LanguageKeywordModel>(LanguageKeywordModel, url).pipe(
//             map((res: LanguageKeywordModel) => {
//                 this._languageKeywords = !res.error && res.data ? res.data : [];
//                 this._storage.setItem(StorageKeys.LANGUAGE_KEYWORDS, res);
//             })
//         ).subscribe();        
//     }
//     async getLanguageKeywords(): Promise<LanguageKeyword[]> {
//         if (!this._languageKeywords) {
//             this._languageKeywords = await this._storage.
//             getItem<LanguageKeywordModel>(StorageKeys.LANGUAGE_KEYWORDS, LanguageKeywordModel).then(
//                 (res: LanguageKeywordModel) =>  { return !res.error && res.data ? res.data : []; }
//             );
//         }
//         return this._languageKeywords;
//     }
// }
