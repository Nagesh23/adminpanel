import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Injectable()
export class RoutingService {

    constructor(private _router: Router) { 
    }

    navigate(commands: any[], extras?: NavigationExtras) {
        this._router.navigate(commands, extras);
    }
}
