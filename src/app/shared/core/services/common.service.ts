import { Injectable, Optional, SkipSelf, ElementRef } from '@angular/core';
import { PhysicianData } from '../../models/physician_data';
import { ClinicInfoList } from '../../models/clinic_info';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { RunAlertService } from '../../services/run-alert.service';
import { ErrorState } from '../../models/error_state';
import { StorageService } from './storage.service';
import { StorageKeys } from '../../models/storage_keys';
import { AdminUserData } from '../../models/adminuser_data';
import { plainToClass } from 'class-transformer';

@Injectable()
export class CommonService {

    private _authorisedPhy: PhysicianData;
    private _authorisedAdminUser: AdminUserData;
    private _phyClinic: ClinicInfoList;
    private _token: string;
    private _loaderState: Subject<boolean> = new Subject<boolean>();
    private _loginState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    constructor(@Optional() @SkipSelf() private _isExist: CommonService, private _storage: StorageService,
                private _alert: RunAlertService) {
        if (this._isExist) {
            throw new Error('Common Service is already imported, should be imported in App Module only...');
        }
        this.authToken ? this.setLoginState(true) : this.setLoginState(false);
    }
    showLoader() {
        this._loaderState.next(true);
    }
    hideLoader() {
        this._loaderState.next(false);
    }
    clearLocalStorage() {
        this._token = this._authorisedPhy = this._phyClinic = null;
        localStorage.clear();
        this._storage.clearStorage();
        this.setLoginState(false);
    }
    get loaderState(): Observable<boolean> {
        return this._loaderState.asObservable();
    }
    setLoginState(isLogin: boolean) {
        this._loginState.next(isLogin);
    }
    get loginState(): Observable<boolean> {
        return this._loginState.asObservable();
    }
    set authToken(token: string) {
        this._token = token;
        localStorage.setItem(StorageKeys.AUTH_TOKEN, this._token);
    }
    get authToken(): string {
        if (!this._token) {
            this._token = localStorage.getItem(StorageKeys.AUTH_TOKEN);
        }
        return this._token;
    }
    set adminUser(adminUser: AdminUserData) {
        this._authorisedAdminUser = adminUser;
        localStorage.setItem(StorageKeys.LOGGED_IN_ADMIN_USER, JSON.stringify(this._authorisedAdminUser));
    }
    get adminUser() {
        if(!this._authorisedAdminUser) {
            this._authorisedAdminUser = plainToClass(AdminUserData, JSON.parse(localStorage.getItem(StorageKeys.LOGGED_IN_ADMIN_USER)));
        }
        return this._authorisedAdminUser;
    }
    set clinic(clinic: ClinicInfoList) {
        this._phyClinic = clinic;
        localStorage.setItem(StorageKeys.PRACTICING_CLINIC, JSON.stringify(this._phyClinic));
    }
    get clinic(): ClinicInfoList {
        if (!this._phyClinic) {
            // this._phyClinic = plainToClass(ClinicInfoList, JSON.parse(localStorage.getItem(StorageKeys.PRACTICING_CLINIC)));
        }
        return this._phyClinic;
    }
    runAlert(error: ErrorState) {
        this._alert.runAlert(error);
    }
}
