import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse } from '@angular/common/http';
import { CommonService } from './common.service';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable()

export class HeaderInterceptorService implements HttpInterceptor {
    constructor(private _comSer: CommonService, private _router: Router) { }
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // let urlModified: HttpRequest<any> = req.withCredentials ? 
        // req.clone({ url: req.url.replace(req.url, environment.snomedUrl + req.url) }) : 
        // req.clone({ url: req.url.replace(req.url, environment.baseUrl + req.url) });
        let urlModified: HttpRequest<any> = req.clone({ url: req.url.replace(req.url, environment.baseUrl + req.url) });
        if (this._comSer.authToken) {
            urlModified = urlModified.withCredentials ? urlModified.clone({ setHeaders: { authToken: '12345678', roleType: '4' } }) :
                urlModified.clone({ setHeaders: { authToken: this._comSer.authToken } });
        }
        return next.handle(urlModified).pipe(tap((res: any) => {
            if (res instanceof HttpResponse) {
                this._comSer.hideLoader();
                if (res.body.error && res.body.code === '401') {
                    this._router.navigate(['login']);
                }
            }
        }, (error) => {
            this._comSer.hideLoader();
        }));
    }
}
