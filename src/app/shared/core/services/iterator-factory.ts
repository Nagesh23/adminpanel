export function* makeIteratorOnKeyOfObject<T>(array: T[], key: string) {
    for (let i = 0; i < array.length; i++) {
        yield array[i][key];
    }    
}
