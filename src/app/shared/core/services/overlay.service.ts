import { ElementRef, ViewContainerRef, ComponentRef, Injectable } from '@angular/core';
import { Overlay, OverlayRef, OverlayConfig, HorizontalConnectionPos, VerticalConnectionPos } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

export enum PositionStrategy {
    FLEXIBLE_TO_ELEMENT,
    RELATIVE_TO_ELEMENT,
    CENTER_TO_VIEW,
}

export interface DialogConfig {
    panelClass?: string;
    hasBackdrop?: boolean;
    backdropClass?: string;
    disableClose?: boolean;
    width?: number | string;
    height?: number | string;
    minWidth?: number | string;
    minHeight?: number | string;
    maxWidth?: number | string;
    maxHeight?: number | string;
    positions?: PositionStrategy;
    originX?: HorizontalConnectionPos;
    originY?: VerticalConnectionPos;
    overlayX?: HorizontalConnectionPos,
    overlayY?: VerticalConnectionPos;
}

const DEFAULT_CONFIG: DialogConfig = {
    hasBackdrop: true,
    backdropClass: 'dark_backdrop',
    panelClass: 'tm-file-preview-dialog-panel',
    disableClose: false,
    positions: PositionStrategy.CENTER_TO_VIEW,
    originX: 'start',
    originY: 'bottom',
    overlayX: 'start',
    overlayY: 'top',
};

export class PopupRef {
    backDropClickSubscn: Subscription;
    constructor(public overlayRef: OverlayRef) { }
    closeDialog() { this.backDropClickSubscn.unsubscribe(); this.overlayRef.dispose(); }
    subscribetoBackDropClick() {
        this.backDropClickSubscn = this.overlayRef.backdropClick().pipe(take(1))
        .subscribe((value) => (this.closeDialog()));
    }
    detachBackDrop() { this.overlayRef.detachBackdrop(); }
    getOverlayConfig() { return this.overlayRef.getConfig(); }
}

@Injectable()

export class OverlayService {

    private _hostEL: ElementRef;
    constructor(private _overlay: Overlay) {}
    open<T, R>(component: ComponentType<T>,  viewContRef: ViewContainerRef = null, config: DialogConfig, 
        connectedToEl?: ElementRef, dataToPass: R = null) {
        this._hostEL = connectedToEl ? connectedToEl : null;
        const dialogConfig = { ...DEFAULT_CONFIG, ...config };
        const overlayRef = this._createOverlay(dialogConfig);
        const filePreviewPortal = new ComponentPortal(component, viewContRef);
        const compRef: ComponentRef<T> = overlayRef.attach(filePreviewPortal);
        const dialogRef = new PopupRef(overlayRef);
        (<any>compRef.instance).popupRef = dialogRef;
        (<any>compRef.instance).data = dataToPass;
        if (!dialogConfig.disableClose) { this._subscribeToOverlayCloseOnClick(dialogRef); }
        return dialogRef;
    }
    private _subscribeToOverlayCloseOnClick(popupRef: PopupRef) {
        popupRef.subscribetoBackDropClick();
    }
    private _createOverlay(config: DialogConfig) {
        const overlayConfig = this._getOverlayConfig(config);
        return this._overlay.create(overlayConfig);
    }
    private _getOverlayConfig(config: DialogConfig): OverlayConfig {
        let positionStrategy;
        if ( config.positions === PositionStrategy.RELATIVE_TO_ELEMENT ) {
            positionStrategy = this._getPSRelativeToEl(config);
        } else if (config.positions === PositionStrategy.CENTER_TO_VIEW) {
            positionStrategy = this._getPSCenterToView();
        } else if (config.positions === PositionStrategy.FLEXIBLE_TO_ELEMENT) {
            positionStrategy = this._getPSFlexibleToEL();
        }
        const overlayConfig = new OverlayConfig({
            hasBackdrop: config.hasBackdrop,
            backdropClass: config.backdropClass,
            panelClass: config.panelClass,
            width: config.width,
            height: config.height,
            minWidth: config.minWidth,
            minHeight: config.minHeight,
            disposeOnNavigation: true,
            scrollStrategy: this._overlay.scrollStrategies.block(),
            positionStrategy
        });
        return overlayConfig;
    }
    private _getPSCenterToView() {
        const positionStrategy = this._overlay.position().global().centerHorizontally().centerVertically();
        return positionStrategy;
    }
    private _getPSRelativeToEl(config: DialogConfig) {
        const positionStrategy = this._overlay.position().connectedTo(this._hostEL,
                { originX: config.originX, originY: config.originY },
                { overlayX: config.overlayX, overlayY: config.overlayY });
        return positionStrategy;
    }
    private _getPSFlexibleToEL() {
        const position = this._overlay.position().flexibleConnectedTo(this._hostEL);
        position.withPush(false);
        position.withFlexibleDimensions(false);
        position.withViewportMargin(8);
        position.withPositions([{ originX: 'start',
        originY: 'bottom',
        overlayX: 'start',
        overlayY: 'top' },
        {
            originX: 'start',
            originY: 'top',
            overlayX: 'start',
            overlayY: 'bottom'
        },
        {
            originX: 'end',
            originY: 'bottom',
            overlayX: 'end',
            overlayY: 'top'
        },
        {
            originX: 'end',
            originY: 'top',
            overlayX: 'end',
            overlayY: 'bottom'
        }]);
        return position;
    }
}
