import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { plainToClass } from 'class-transformer';
import { ClassType } from './http-request.service';

@Injectable()
export class StorageService {

    constructor(private _storage: Storage) { }
    async getItem<T>(key: string, classType: ClassType<T>, callback?: (savedData: T) => void) {
        const data: T = await this._storage.get(key);
        const classObj: T = plainToClass(classType, data);
        if (typeof callback === 'function') {
            callback((classObj as any));
        }
        return classObj;
    }
    async setItem<T>(key: string, dataToSave: any, callback?: (savedData: T) => void) {
        const data: T = await this._storage.set(key, dataToSave);
        if (typeof callback === 'function') {
            callback(data);
        }
        return data;
    }
    clearStorage() {
        this._storage.clear();
    }
}
