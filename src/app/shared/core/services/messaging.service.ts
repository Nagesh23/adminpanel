import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { HttpRequestService } from './http-request.service';
import { HttpURLS } from '../../models/api_urls';
import { Response } from '../../models/base_response';
import { CommonService } from './common.service';
import { CoreModule } from '../core.module';

export interface PushNotificationData {
    collapse_key: string;
    data: {
        title: string,
        description: string,
        type: string,
        parentType: string;
        metadata: string;
    }
    from: string;
}

export enum NotificationOrigin {
    APPOINTMENT = '1'
}

@Injectable()
export class MessagingService {
    _token: string;
    currentMessage = new Subject<PushNotificationData>();
    constructor(private _fireDB: AngularFireDatabase, private _fireAuth: AngularFireAuth,
        private _fireMessaging: AngularFireMessaging, private _http: HttpRequestService,
        private _comSer: CommonService) {
        this._fireMessaging.messaging.subscribe(
            (_messaging) => {
                _messaging.onMessage = _messaging.onMessage.bind(_messaging);
                _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
            }
        )
        this.receiveMessage();
    }
    sendToken(token) {
        this._http.postResponse<Response>(Response, HttpURLS.REFRESH_FCM_TOKEN,
            {token: token, sourceType: 1}).pipe(
            map((res: Response) => {
                // console.log(res);
                if (!res.error) {
                    this.receiveMessage();
                } else {
                    this._http.runAlert({ status: true, title: 'PROBLEM IN REFRESHING TOKEN', description: res.message });
                    throw new Error(res.message);
                }
            })
        ).subscribe();
    }
    requestPermission() {
        this._fireMessaging.requestToken.subscribe(
            (token) => {
                // console.log(token);
                this._token = token;
                if (token) { this.sendToken(token); }
            },
            (err) => { alert('Unable to get permission to notify.' + err); }
        );
    }
    receiveMessage() {
        // console.log('receive message called');
        this._fireMessaging.messages.subscribe(
        (payload: PushNotificationData) => {
            console.log('message received ', payload);
            this.currentMessage.next(payload);
        })
    }
    deleteToken() {
        if(this._token) {
            this._fireMessaging.deleteToken(this._token).subscribe((success: boolean) => {
                console.log('token deleted');
            });
        }
    }
}
