import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { Response } from 'src/app/shared/models/base_response';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {

    loader: boolean;
    constructor(private _http: HttpRequestService) { }

    ngOnInit() { }
    setSearchFilters(value: any) {
        this.loader = true;
        const body = { appointmentsFilter: value }
        this._http.postResponse<Response>(Response, HttpURLS.EXPORT_APPTS, body).pipe(
            map((res: Response) => {
                this.loader = false;
                console.log('export res =', res);
                if (!res.error && res.data) { window.open(res.data, '_newtab') };
                this._http.runAlert({
                    status: res.error, title: res.error ? 'FAILED' : 'SUCCESS',
                    description: res.error ? 'Please try again...' : 'Appointments exported Successfully'
                });
            })
        ).subscribe();
    }
}
