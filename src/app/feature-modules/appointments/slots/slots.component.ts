import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppointmentData, TimeSlot } from 'src/app/shared/models/appointment_data';
import { ClinicSlot, DateWiseSlots } from 'src/app/shared/models/slots';
import { SlotsService } from '../services/slots.service';

@Component({
    selector: 'app-slots',
    templateUrl: './slots.component.html',
    styleUrls: ['./slots.component.scss'],
    providers: [SlotsService]
})
export class SlotsComponent implements OnInit {

    loader: boolean = true;
    clinicSlot$: Observable<ClinicSlot>;
    @Input() appointment: AppointmentData;
    date: FormControl = new FormControl(new Date());
    @Output() slotSelected: EventEmitter<TimeSlot> = new EventEmitter<TimeSlot>();
    constructor(private _slotsSer: SlotsService, private _modalCT: ModalController) { }
    ngOnInit() { this.syncSlots(); }
    syncSlots(date: string = this.appointment.date) {
        this.clinicSlot$ = this._slotsSer.phySlots(this.appointment.clinicId,
            this.appointment.physicianId, date).pipe(
                map((dateWiseSlots: DateWiseSlots) => {
                    for (let i = 0; i < dateWiseSlots.clinicSlots.length; i++) {
                        if (this.appointment.clinicId === dateWiseSlots.clinicSlots[i].clinicData.clinicId) {
                            return dateWiseSlots.clinicSlots[i];
                        }
                    }
                    return null;
                }),
                tap(() => { this.loader = false; }),
            );
    }
    dismiss(timeSlot: TimeSlot) { this._modalCT.dismiss(timeSlot); }
    selectedDate(value: any) { this.loader = true; this.syncSlots(value); }
    selectedSlot(timeSlot: TimeSlot) { this.dismiss(timeSlot); }
}
