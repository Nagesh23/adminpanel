import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AppointmentsPage } from './appointments.page';
import { SlotsComponent } from './slots/slots.component';
import { DateTimeModule } from 'src/app/shared/widgets_module/date-time/date-time.module';
import { ReportsComponent } from './reports/reports.component';
import { ApptsFilterComponent } from './appts-filter/appts-filter.component';
import { MaterialModule } from 'src/app/shared/widgets_module/material/material.module';

const routes: Routes = [
    {
        path: '', component: AppointmentsPage, children: [
            { path: '', redirectTo: 'reports', pathMatch: 'full' },
            { path: 'reports', component: ReportsComponent }
        ]
    }
];

@NgModule({
    imports: [
        IonicModule, DateTimeModule, MaterialModule,
        RouterModule.forChild(routes)
    ],
    exports: [SlotsComponent, ApptsFilterComponent],
    declarations: [AppointmentsPage, SlotsComponent, ReportsComponent, ApptsFilterComponent],
    entryComponents: [SlotsComponent, ApptsFilterComponent]
})
export class AppointmentsPageModule { }
