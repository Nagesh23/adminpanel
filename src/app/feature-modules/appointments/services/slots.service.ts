import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { Response } from 'src/app/shared/models/base_response';
import { DateWiseSlots, DateWiseSlotsRes } from 'src/app/shared/models/slots';

@Injectable()
export class SlotsService {

    constructor(private _http: HttpRequestService) { }

    phySlots(clinicId: string, physicianId: string, date: string,
        limit: number = 1, offset: number = 0, remoteSlotsOnly: boolean = true): Observable<DateWiseSlots> {
        const remoteSlotsOnlyP = '&remoteSlotsOnly=' + remoteSlotsOnly;
        const url = HttpURLS.PHYSICIAN_APPTS_SLOTS + physicianId + '&clinicId=' + clinicId + '&fromDate=' +
            date + '&limit=' + limit + '&offset=' + offset + '&remoteSlotsOnly=' + remoteSlotsOnly + '&showAllSlots=' + true;
        return this._http.getResponse<DateWiseSlotsRes>(DateWiseSlotsRes, url).pipe(
            map((res: DateWiseSlotsRes) => { return !res.error && res.data ? res.data[0] : null; })
        );
    }
}