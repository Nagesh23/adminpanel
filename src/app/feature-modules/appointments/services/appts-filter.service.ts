import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { ClinicInfoList, ClinicModel } from 'src/app/shared/models/clinic_info';
import { PhysicianData, PhysiciansQuoClinicRes } from 'src/app/shared/models/physician_data';

@Injectable()

export class ApptsFilterService {

    constructor(private _http: HttpRequestService) { }

    allClinics(): Observable<ClinicInfoList[]> {
        return this._http.getResponse<ClinicModel>(ClinicModel, HttpURLS.ALL_CLINICS).pipe(
            map((res: ClinicModel) => !res.error && res.data ? res.data : [])
        );
    }
    physicians(clinicId: string): Observable<PhysicianData[]> {
        const url = HttpURLS.PHY_BY_CLINIC + clinicId;
        return this._http.getResponse<PhysiciansQuoClinicRes>(PhysiciansQuoClinicRes, url).pipe(
            map((res: PhysiciansQuoClinicRes) => !res.error && res.data ? res.data.physiciansList : [])
        )
    }
}