import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material';
import { Moment } from 'moment';
import { EMPTY, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap, startWith, switchMap, tap } from 'rxjs/operators';
import { stringForbiddenValidator } from 'src/app/shared/core/directives/string_forbidden.directive';
import { ClinicInfoList } from 'src/app/shared/models/clinic_info';
import { PhysicianData } from 'src/app/shared/models/physician_data';
import { ApptsFilterService } from '../services/appts-filter.service';

export function cacellationCharge(notToExceed: number) {
    return (c: AbstractControl): { [key: string]: any } => {
        if (c.value) {
            const isExceeding = parseInt(c.value, 10) > notToExceed;
            if (isExceeding) {
                return { 'exceeding': { valid: false } };
            } else {
                return null;
            }
        }
    };
}


@Component({
    selector: 'appts-filter',
    templateUrl: './appts-filter.component.html',
    styleUrls: ['./appts-filter.component.scss'],
    providers: [ApptsFilterService]
})
export class ApptsFilterComponent implements OnInit, AfterViewInit {

    filterFG: FormGroup;
    clinics$: Observable<ClinicInfoList[]>
    physicians$: Observable<PhysicianData[]>;
    @Input() buttonName: string = 'Search';
    @Output() filter: EventEmitter<{}> = new EventEmitter<{}>();
    @ViewChild('filterClinic', { static: false }) filterClinic: MatAutocomplete;
    constructor(private _fb: FormBuilder, private _filterSer: ApptsFilterService,
        private _dateAd: DateAdapter<Moment>) { }

    ngOnInit() {
        this.filterFG = this._fb.group({
            physicianId: ['', [Validators.required]],
            fromDate: [new Date()],
            toDate: [new Date()],
            patientName: [''],
            phoneNumber: ['', [stringForbiddenValidator()]],
            showMROnly: [false],
            quickFilterCheck: [],
            appointmentStatusList: [],
            physicianIdList: [],
            offset: 0,
            limit: 50,
            clinicId: [''],
            appointmentType: [null]
        });
        this.clinics$ = this._filterSer.allClinics().pipe(
            mergeMap((clinics: ClinicInfoList[]) => {
                return this.filterFG.get('clinicId').valueChanges.pipe(
                    debounceTime(50), distinctUntilChanged(), startWith(''),
                    switchMap((query: any) => {
                        if (typeof query === 'string') {
                            return of(clinics.filter((clinic: ClinicInfoList) => {
                                return clinic.name.toLowerCase().includes(query.toLowerCase())
                            }))
                        } else if (typeof query === 'object' && query instanceof ClinicInfoList) {
                            return of(clinics.filter((clinic: ClinicInfoList) => {
                                return clinic.name.toLowerCase().includes(((<ClinicInfoList>query).name).toLowerCase())
                            }))
                            // return EMPTY;
                        } else {
                            return EMPTY;
                        }
                    })
                )
            })
        );
    }
    ngAfterViewInit() {
        this.physicians$ = this.filterClinic.optionSelected.pipe(
            switchMap((option: MatAutocompleteSelectedEvent) => {
                const clinic: ClinicInfoList = option.option.value;
                return this._filterSer.physicians(clinic.clinicId);
            })
        )
    }
    displayClinicByName(clinic: ClinicInfoList) { if (clinic) { return clinic.name; } }
    reset() {
        this.filterFG.reset({
            fromDate: [new Date()],
            toDate: [new Date()],
            showMROnly: [false],
        });
    }
    emitValues() {
        const data2Emit = this.filterFG.getRawValue();
        data2Emit.fromDate = this._dateAd.format(this._dateAd.parse(data2Emit.fromDate, 'DD-MM-YYYY'), 'DD-MM-YYYY');
        data2Emit.toDate = this._dateAd.format(this._dateAd.parse(data2Emit.toDate, 'DD-MM-YYYY'), 'DD-MM-YYYY');
        const clinic = this.filterFG.get('clinicId').value;
        data2Emit.clinicId = typeof clinic === 'object' && clinic instanceof ClinicInfoList ? clinic.clinicId : null;
        console.log(data2Emit);
        this.filter.emit(data2Emit);
    }
}
