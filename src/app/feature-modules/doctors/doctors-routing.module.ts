import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './doctors.common';

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule { }
