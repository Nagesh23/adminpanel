import { NgModule } from '@angular/core';
import { DoctorsRoutingModule } from './doctors-routing.module';
import { modulesImports, providerDeclarations, componentDeclarations } from './doctors.common';


@NgModule({
  imports: [DoctorsRoutingModule, ...modulesImports],
  providers: [...providerDeclarations],
  declarations: [...componentDeclarations]
})
export class DoctorsPageModule {}
