import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/core/services/common.service';
import { PhysicianListService } from '../services/physician-list.service';
import { PhysicianData, PhysiciansResponse } from 'src/app/shared/models/physician_data';
import { PhysicianAuthoriseModel } from 'src/app/shared/models/physician_authorise_model';
import { Response } from 'src/app/shared/models/base_response';

@Component({
  selector: 'app-physician-list',
  templateUrl: './physician-list.component.html',
  styleUrls: ['./physician-list.component.scss'],
})
export class PhysicianListComponent implements OnInit {
    searchPhysicianForm: FormGroup;
    physicians: PhysicianData[];
    loader = false;
    totalCount: number;
    limit: number;
    offset: number;
    myPhysicians = {
        physicianName: '',
        isAuthorised: '',
        offset: 0,
        limit: 10
    }
    constructor(private comSer: CommonService, private fb: FormBuilder, private phySer: PhysicianListService) {
        this.initialiseSearchPhyForm();
    }
    initialiseSearchPhyForm() {
        this.searchPhysicianForm = this.fb.group({
            physicianName: [''],
            isAuthorised: [''],
            offset: [0, ''],
            limit: [10, '']
      	}); 
    }
    initialiseMyPhysiciansJson() {
        this.myPhysicians = {
            physicianName: '',
            isAuthorised: '',
            offset: 0,
            limit: 10
        }
    }
    ngOnInit() {}

    ionViewDidEnter() {
        this.syncPhysiciansList();
    }
    syncPhysiciansList() {  
        this.loader = false;
        this.comSer.showLoader();
        console.log(this.myPhysicians);
        this.phySer.getPhysiciansListFromServer(this.myPhysicians).subscribe((res: PhysiciansResponse) => {
            this.physicians = res.data ? res.data : [];
            this.comSer.hideLoader();
            this.totalCount = res.totalCount;
            this.loader = true;
            this.filterPhysicians();
        });
    }
    authoriseOrUnauthorisePhysician(physician: PhysicianData) {
        const phy_authorise = new PhysicianAuthoriseModel(); 
        physician.authorised ? phy_authorise.authorise = false : phy_authorise.authorise = true;
        phy_authorise.physicianId = physician.physicianId;
        
        this.loader = false;
        this.comSer.showLoader();
        this.phySer.authoriseOrUnauthorisePhysicianFromServer(phy_authorise).subscribe((res: Response) => {
            this.comSer.hideLoader();
        }, (error => {
            
        }), () => {
            this.syncPhysiciansList();
        });
    }
    filterPhysicians() {
        if(this.physicians) {
            this.physicians.filter((physician) => {
                if(physician) {
                    physician.authorised ? physician.authorisedName = 'Authorised': physician.authorisedName = 'Un-Authorised'
                }
            });
        }
    }
    getFilteredMyPhysiciansByAdminUser() {
        this.myPhysicians.physicianName = this.searchPhysicianForm.get('physicianName').value;
		this.myPhysicians.isAuthorised = this.searchPhysicianForm.get('isAuthorised').value;
        this.syncPhysiciansList();
    }
    resetSearchForm() {
		this.searchPhysicianForm.reset();
		this.initialiseMyPhysiciansJson();
		this.getFilteredMyPhysiciansByAdminUser();
    }
    getNextList(event) {
		this.myPhysicians.offset = event.pageIndex * this.myPhysicians.limit;
		this.getFilteredMyPhysiciansByAdminUser();
    }
}
