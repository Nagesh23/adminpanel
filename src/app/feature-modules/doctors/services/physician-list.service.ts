import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PhysicianResponse, PhysicianData, PhysiciansResponse } from 'src/app/shared/models/physician_data';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { map } from 'rxjs/operators';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { BaseResponse, Response } from 'src/app/shared/models/base_response';
import { PhysicianAuthoriseModel } from 'src/app/shared/models/physician_authorise_model';

@Injectable()
export class PhysicianListService {

    constructor(private _http: HttpRequestService) { }

    getPhysiciansListFromServer(params): Observable<PhysiciansResponse | Error> {
        return this._http.postResponse<PhysiciansResponse>(PhysiciansResponse, HttpURLS.FILTERED_PHYSICIANS_LIST,
            params).pipe(
            map((res: PhysiciansResponse) => {
                if (!res.error) {
                    return res;
                } else {
                    this._http.runAlert({ status: true, title: 'PROBLEM IN FETCHING PHYSICIANS.', description: res.message });
                    throw new Error(res.message);
                }
            })
      );
    }
    authoriseOrUnauthorisePhysicianFromServer(phy_authorise: PhysicianAuthoriseModel): Observable<Response | Error> {
        return this._http.patchResponse<Response>(Response, HttpURLS.AUTHORISE_PHYSICIAN,
            phy_authorise).pipe(
            map((res: Response) => {
                if(!res.error){
                    return res.data;
                } else {
                    this._http.runAlert({ status: true, title: 'PROBLEM IN UPDATING PHYSICIAN.', description: res.message });
                    throw new Error(res.message);
                }
            })
        );
    }
}
