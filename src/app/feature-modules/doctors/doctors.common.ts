import { Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from 'src/app/shared/widgets_module/material/material.module';
import { CoreModule } from 'src/app/shared/core/core.module';
import { DateTimeModule } from 'src/app/shared/widgets_module/date-time/date-time.module';
import { PhysicianListComponent } from './physician-list/physician-list.component';
import { AuthLoginGuard } from 'src/app/shared/core/services/auth-login.guard';
import { DoctorsPage } from './doctors.page';
import { PhysicianListService } from './services/physician-list.service';

export const componentDeclarations: any[] = [DoctorsPage, PhysicianListComponent];
export const providerDeclarations: any[] = [PhysicianListService];
export const modulesImports: any[] = [MaterialModule, IonicModule, CoreModule, DateTimeModule];
export const entryComponents: any[] = [];
export const routes: Routes = [
    { path: '', component: DoctorsPage, children: [
        { path: '', redirectTo: 'physicianList', pathMatch: 'full' },
        { path: 'physicianList', component: PhysicianListComponent, canActivate: [AuthLoginGuard] }
    ]}
];
