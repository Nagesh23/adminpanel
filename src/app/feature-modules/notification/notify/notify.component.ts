import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/core/services/common.service';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss']
})
export class NotifyComponent implements OnInit {

    constructor(public commonService: CommonService) { }
  
    ngOnInit() {
    }

}
