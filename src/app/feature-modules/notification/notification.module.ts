import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { componentDeclarations, exportsDeclarations } from './notification.common';
import { MaterialModule } from 'src/app/shared/widgets_module/material/material.module';

@NgModule({
  declarations: [...componentDeclarations],
  imports: [
    CommonModule, MaterialModule
  ],
  exports: [...exportsDeclarations]
})
export class NotificationModule { }
