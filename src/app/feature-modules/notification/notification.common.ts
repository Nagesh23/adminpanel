import { Routes } from '@angular/router';
import { BellComponent } from './bell/bell.component';
import { LogoutComponent } from './logout/logout.component';
import { NotifyComponent } from './notify/notify.component';

export const componentDeclarations: any[] = [
    BellComponent, LogoutComponent, NotifyComponent
];

export const providerDeclarations: any[] = [
];

export const exportsDeclarations: any[] = [NotifyComponent];

export const routes: Routes = [
];
