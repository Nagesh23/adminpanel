import { Component, OnInit } from '@angular/core';
import { PhysicianData } from 'src/app/shared/models/physician_data';
import { CommonService } from 'src/app/shared/core/services/common.service';
import { RoutingService } from 'src/app/shared/core/services/routing.service';
import { IonRoutingService } from 'src/app/shared/core/services/ionic-routing.service';
import { AdminUserData } from 'src/app/shared/models/adminuser_data';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

    adminUser: AdminUserData;
    constructor(private _comSer: CommonService, private _router: IonRoutingService) {}
    ngOnInit() {
        this.adminUser = this._comSer.adminUser;
    }
    logout() {
        this._comSer.clearLocalStorage();
        this._router.navigateRoot(['/login']);
    }
}
