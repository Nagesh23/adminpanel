import { Component, OnInit, Input } from '@angular/core';
import { OnlineUsersData, LoginUsersData } from 'src/app/shared/models/online-users_response';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-online-users',
  templateUrl: './online-users.component.html',
  styleUrls: ['./online-users.component.scss'],
})
export class OnlineUsersComponent implements OnInit {
    @Input() data: {flow: number, userData: OnlineUsersData};
    constructor(private _modalCt: ModalController) { }
  
    ngOnInit() {
        if(this.data && this.data.flow===2){
            // this.filterUserData();
        }
    }
    filterUserData() {
        if(this.data.userData && this.data.userData.loginUsersList) {
            this.data.userData.loginUsersList.filter((value) => {
                if(value && value.userName){
                    value.userName = this.getUsername(value).join('');
                }
                if(value && value.phoneNumber){
                    value.phoneNumber = this.getPhonenumber(value).join('');
                }
            });
        }
    }
    getUsername(value: LoginUsersData) {
        let tempname = [...value.userName];
        // console.log(tempname);
        for(let i = 1 ; i < tempname.length ; i++) {
            if(tempname[i] !== ' ') {
                tempname[i] = '*';
            } else {
                i++;
            }
        }
        return tempname;
    }
    getPhonenumber(value: LoginUsersData) {
        let tempnumber = [...value.phoneNumber];
        for(let i = 1 ; i < 8 ; i++) {
            tempnumber[i] = '*';
        }
        return tempnumber;
    }
    dismissModal() {
        this._modalCt.dismiss();
    }
}
