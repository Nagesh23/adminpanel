import { Injectable } from '@angular/core';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { map } from 'rxjs/operators';
import { OnlineUsersResponseModel, OnlineUsersData } from 'src/app/shared/models/online-users_response';
import { Observable } from 'rxjs';

@Injectable()
export class OnlineUsersService {
    
    constructor(private _httpSer: HttpRequestService){}

    public _getOnlineUsersByRoleForAdminUser(role: number): Observable<OnlineUsersData | Error> {
        return this._httpSer.getResponse<OnlineUsersResponseModel>(OnlineUsersResponseModel, HttpURLS.GET_ONLINE_USERS_BY_ROLE + role)
        .pipe(
            map((res: OnlineUsersResponseModel) => {
                if (!res.error) {
                    return res.data;
                } else {
                    this._httpSer.runAlert({ status: true, title: 'PROBLEM IN FETCHING APPOINTMENTS.', description: res.message });
                    throw new Error(res.message);
                }
            })
        );
    }
}
