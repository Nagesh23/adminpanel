import { Injectable } from '@angular/core';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { StorageService } from 'src/app/shared/core/services/storage.service';
import { AppointmentData, AppointmentsResponse } from 'src/app/shared/models/appointment_data';
import { StorageKeys } from 'src/app/shared/models/storage_keys';
import { Observable, iif, EMPTY } from 'rxjs';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { map, catchError, take } from 'rxjs/operators';
import { Response } from 'src/app/shared/models/base_response';

@Injectable()
export class AppointmentService {
    private _appointmentList: AppointmentData[];

    constructor(private _http: HttpRequestService, private _storage: StorageService) { }

    updateAppt(appointment: AppointmentData) {
        return this._http.putResponse<Response>(Response, HttpURLS.UPDATE_APPOINTMENT, appointment).pipe(
            map((res: Response) => {
                return !res.error && res.data ? (this._http.runAlert({
                    status: false, title: 'SUCCESS', description: res.message
                }), !!res.data) : (this._http.runAlert({
                    status: true, title: 'FAILED!', description: res.message
                }), false)
            })
        );
    }

    private async _getAppointmentListFromStorage(): Promise<AppointmentData[]> {
        if (!this._appointmentList) {
            this._appointmentList = await this._storage.getItem(StorageKeys.APPOINTMENT_LIST, AppointmentsResponse).then((res: AppointmentsResponse) => {
                return res && res.data ? res.data : [];
            });
        }
        return this._appointmentList;
    }
    private _getAppointmentListFromServer(params): Observable<AppointmentData[] | Error> {
        // const phyId = this._comSer.adminUser.adminUserId;

        return this._http.postResponse<AppointmentsResponse>(AppointmentsResponse, HttpURLS.FILTERED_REMOTE_APPOINTMENT_LIST,
            params).pipe(
                map((res: AppointmentsResponse) => {
                    if (!res.error) {
                        this._storage.setItem<AppointmentsResponse>(StorageKeys.APPOINTMENT_LIST, res);
                        this._appointmentList = res.data ? res.data : [];
                        return this._appointmentList;
                    } else {
                        this._http.runAlert({ status: true, title: 'PROBLEM IN FETCHING APPOINTMENTS.', description: res.message });
                        throw new Error(res.message);
                    }
                })
            );
    }
    getAppointmentList(params): Observable<AppointmentData[]> {
        /** 
         * ```ts 
         * use merge operator if want to work subscriber parallely... (Observables will not wait for the previous one to complete)....
         */
        return iif(() =>
            navigator.onLine, this._getAppointmentListFromServer(params), this._getAppointmentListFromStorage()
        ).pipe(
            catchError((error) => { return EMPTY; }),
            map((appointments: AppointmentData[]) => {
                return appointments;
            }), take(1)
        );
    }
}

