import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { modulesImports, componentDeclarations, providerDeclarations, entryComponents } from './home.common';

@NgModule({
  imports: [HomeRoutingModule, ...modulesImports],
  providers: [...providerDeclarations],
  declarations: [...componentDeclarations],
  entryComponents: [...entryComponents]
})
export class HomePageModule {}
