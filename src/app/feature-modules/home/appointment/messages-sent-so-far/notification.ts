import { Type } from 'class-transformer';
import { BaseResponse } from 'src/app/shared/models/base_response';

export class NotificationResponse extends BaseResponse {
    error: boolean;
    code: string;
    @Type(() => Notification)
    data: Notification[];
    message: string;
}

export class Notification {
    created: any;
    updated: any;
    createdBy: string;
    updatedBy: string;
    version: number;
    userId: string;

    /**
     * PHYSICIAN(1,"Physician"),
        PATIENT(2,"Patient"),
        RECEPTIONIST(3,"Receptionist"),
        ADMIN(4,"Admin"),
        LAB_USER(5,"Lab User"),
        PHARMACY_USER(6,"Pharmacy User"),
        SYSTEM(7,"System"),
        MARKETING_USER(8,"Marketing User"),
        CUSTOMER_SUPPORT(9, "Customer Support");
     */

    roleType: number;
    token: string;
    serverKey: string;
    title: string;
    description: string;
    metaData: string;
    /**
     * SUCCESS = 1,
     * FAILED = 2,
     */
    pushStatus: number;
    referenceId: string;
    clinicId: string;
    physicianId: string;
    pushNotificationId: string;
    sms: string;
    messageStatus: number;
    /**
     * if pushstatus is 2 then holds some value.
     */
    response: string;
}

