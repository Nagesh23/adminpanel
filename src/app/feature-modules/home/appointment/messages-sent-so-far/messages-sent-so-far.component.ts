import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { Response } from 'src/app/shared/models/base_response';
import { Notification, NotificationResponse } from './notification';

@Component({
  selector: 'app-messages-sent-so-far',
  templateUrl: './messages-sent-so-far.component.html',
  styleUrls: ['./messages-sent-so-far.component.scss'],
})
export class MessagesSentSoFarComponent implements OnInit {

  @Input() apptId: string;
  @Input() patientId: string;
  msgs$: Observable<Notification[]>;
  constructor(private _modalCT: ModalController, private _http: HttpRequestService) { }

  ngOnInit() { this.getSmsNotfns(); }
  dismiss() { this._modalCT.dismiss(); }

  getSmsNotfns() {
    const url = HttpURLS.USER_NOTIFICATIONS + this.apptId;
    this.msgs$ = this._http.getResponse<NotificationResponse>(NotificationResponse, url).pipe(
      map((res: Response) => {
        console.log(res);
        return !res.error && res.data ? res.data : [];
      })
    );
  }
}
