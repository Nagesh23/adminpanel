import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { EMPTY } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';
import { AppointmentData } from 'src/app/shared/models/appointment_data';
import { Response } from 'src/app/shared/models/base_response';

export function cacellationCharge(notToExceed: number) {
    return (c: AbstractControl): { [key: string]: any } => {
        if (c.value) {
            const isExceeding = parseInt(c.value, 10) > notToExceed;
            if (isExceeding) {
                return { 'exceeding': { valid: false } };
            } else {
                return null;
            }
        }
    };
}

@Component({
    selector: 'app-refund',
    templateUrl: './refund.component.html',
    styleUrls: ['./refund.component.scss'],
})
export class RefundComponent implements OnInit {

    loader: boolean;
    refundFG: FormGroup;
    amountToRefund: number = 0;
    @Input() appointment: AppointmentData;
    constructor(private _fb: FormBuilder, private _modalCT: ModalController,
        private _http: HttpRequestService) { }

    ngOnInit() {
        this.refundFG = this._fb.group({
            referenceId: [this.appointment.appointmentId],
            charges: ['', [Validators.required, cacellationCharge(this.appointment.amount)]],
            refundNote: ['', Validators.required],
        });
        this.refundFG.get('charges').valueChanges.pipe(
            distinctUntilChanged(), debounceTime(50),
            switchMap((charges: string) => {
                this.amountToRefund = this.appointment.amount - parseInt(charges);
                return EMPTY;
            })
        ).subscribe();
    }
    dismiss(value: boolean = false) { this._modalCT.dismiss(value); }
    submit() {
        this.loader = true;
        this._http.postResponse<Response>(Response, HttpURLS.REQUEST_REFUND, this.refundFG.getRawValue()).
            subscribe((res: Response) => {
                this._http.runAlert({
                    status: res.error, title: res.error ? 'FAILED!' : 'SUCCESS', description: res.message
                });
                this.loader = false; this.dismiss(!res.error);
            });
    }
}
