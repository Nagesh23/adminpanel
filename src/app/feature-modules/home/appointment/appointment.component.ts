import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppointmentData, TimeSlot } from 'src/app/shared/models/appointment_data';
import { AppointmentService } from '../services/appointment.service';
import { interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { AlertController, ModalController } from '@ionic/angular';
import { SlotsComponent } from '../../appointments/slots/slots.component';
import { RefundComponent } from './refund/refund.component';
import { MessagesSentSoFarComponent } from './messages-sent-so-far/messages-sent-so-far.component';

@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.scss'],
})
export class AppointmentComponent implements OnInit {

    @Input() appointment: AppointmentData;
    countDownForPaidAppo$: Observable<any>;
    countDownForBookedAppo$: Observable<any>;
    warningForPaidAppo: number;
    warningForBookedAppo: number;
    @Output() refresh: EventEmitter<void> = new EventEmitter<void>();
    constructor(private _apptSer: AppointmentService,
        private _modalCT: ModalController, private _alertCT: AlertController) { }
    ngOnInit() {
        console.log(this.appointment);
        this.initialiseCountdown();
    }
    async showSentSmsNPushNotn() {
        const modalEL = await this._modalCT.create({
            component: MessagesSentSoFarComponent,
            componentProps: {
                apptId: this.appointment.appointmentId,
                patientId: this.appointment.patientId
            }
        });
        await modalEL.present();
    }
    async initiateRefund() {
        const modalEL = await this._modalCT.create({
            cssClass: ['refundModal'],
            component: RefundComponent,
            componentProps: { appointment: this.appointment }
        });
        await modalEL.present();
        const { data } = await modalEL.onDidDismiss();
        if (data) { this.refresh.emit(); }
    }
    async pickSlot() {
        const modalEL = await this._modalCT.create({
            component: SlotsComponent,
            componentProps: { appointment: this.appointment }
        });
        await modalEL.present();
        const { data } = await modalEL.onDidDismiss();
        if (data) { this.rescheduleAppt(data); }
    }
    rescheduleAppt(timeSlot: TimeSlot) {
        this.appointment.loader = true;
        this.appointment.appointmentStatus = 7;
        this.appointment.timeSlot = timeSlot;
        this._apptSer.updateAppt(this.appointment).subscribe(
            (success: boolean) => { this.appointment.loader = false; this.refresh.emit(); }
        );
    }
    async cancelAppt() {
        const alert = await this._alertCT.create({
            header: 'Confirm!',
            message: 'Do you want to <strong>Cancel</strong> this Appointment ?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                }, {
                    text: 'Okay',
                    handler: () => {
                        this.appointment.loader = true;
                        this.appointment.appointmentStatus = 5;
                        this._apptSer.updateAppt(this.appointment).subscribe(
                            (success: boolean) => { this.appointment.loader = false; this.refresh.emit(); }
                        );
                    }
                }
            ]
        });
        await alert.present();
    }
    initialiseCountdown() {
        if (this.appointment && this.appointment.appointmentStatus === 12) {
            this.countdownForPaidAppointment();
        } else if (this.appointment && this.appointment.appointmentStatus === 4) {
            this.countDownForBookedAppointment();
        }
    }
    countdownForPaidAppointment() {
        this.countDownForPaidAppo$ = interval(10000);
        this.countDownForPaidAppo$ = this.countDownForPaidAppo$.pipe(map((value) => {
            return this.getDurationOfPaidAppoi();
        }));
    }
    countDownForBookedAppointment() {
        this.countDownForBookedAppo$ = interval(10000);
        this.countDownForBookedAppo$ = this.countDownForBookedAppo$.pipe(map((value) => {
            return this.getDurationOfBookedAppoi();
        }));
    }
    getDurationOfPaidAppoi() {
        const differenceInMilli = this.getCurrentDateToMilliSec() - this.appointment.created;
        const differenceInSeconds = differenceInMilli / 1000;
        const differenceInMinites = differenceInSeconds / 60;
        const differenceInHours = differenceInMinites / 60;
        const differenceInDays = differenceInHours / 24;
        if (differenceInMinites < 20) {
            this.warningForPaidAppo = 1;
        }
        if (differenceInMinites > 20 && differenceInMinites < 30) {
            this.warningForPaidAppo = 2;
        }
        if (differenceInMinites > 30) {
            this.warningForPaidAppo = 3;
        }
        if (differenceInSeconds < 60) {
            return Math.floor(differenceInSeconds) + ' sec Elapsed';
        } else if (differenceInMinites < 60) {
            return Math.floor(differenceInMinites) + ' min Elapsed';
        } else if (differenceInHours < 24) {
            return Math.floor(differenceInHours) + ' hrs Elapsed';
        } else {
            return Math.floor(differenceInDays) + ' days Elapsed';
        }
    }
    getDurationOfBookedAppoi() {
        if (this.appointment && this.appointment.timeSlot && this.appointment.timeSlot.startDateTime) {
            const differenceInMilli = this.appointment.timeSlot.time
                - this.getCurrentDateToMilliSec();
            let differenceInSeconds = differenceInMilli / 1000;
            let differenceInMinites = differenceInSeconds / 60;
            let differenceInHours = differenceInMinites / 60;
            let differenceInDays = differenceInHours / 24;
            let flag = false;
            let message = '';

            if (differenceInSeconds < 0) {
                differenceInSeconds = differenceInSeconds * -1;
                differenceInMinites = differenceInMinites * -1;
                differenceInHours = differenceInHours * -1;
                differenceInDays = differenceInDays * -1;
                flag = true;
            }
            if (differenceInMinites > 20) {
                this.warningForBookedAppo = 1;
            }
            if (differenceInMinites > 10 && differenceInMinites < 20) {
                this.warningForBookedAppo = 2;
            }
            if (differenceInMinites < 10) {
                this.warningForBookedAppo = 3;
            }
            if (differenceInSeconds < 60 && differenceInSeconds > 0) {
                message = Math.floor(differenceInSeconds) + ' sec';
            } else if (differenceInMinites < 60 && differenceInMinites > 0) {
                message = Math.floor(differenceInMinites) + ' min';
            } else if (differenceInHours < 24 && differenceInHours > 0) {
                message = Math.floor(differenceInHours) + ' hrs';
            } else if (differenceInDays > 0) {
                message = Math.floor(differenceInDays) + ' days';
            }
            if (flag) {
                this.warningForBookedAppo = 3;
                message = message + ' overdue';
            } else {
                message = message + ' to go';
            }
            return message;
        }
    }
    getCurrentDate(format: any) {
        return moment().format(format);
    }
    getCurrentDay(date: any) {
        return moment(date).weekday();
    }

    getCurrentDateToMilliSec() {
        return moment().valueOf();
    }
    getDateToMilliSec(date: string, inputFormat: string) {
        return moment(date, inputFormat).valueOf();
    }
    milliSecToDays(ms: number) {
        return moment.duration(ms).days();
    }


    msToTimeConversion(msTime: number, format: string) {
        return moment(msTime).format(format);
    }

    getFormattedDate(date: string, inputFormat: string, format: string) {
        return moment(date, inputFormat).format(format);
    }

    convertToDateObject(dateString: string) {
        const dateArray = dateString.split("/");
        const dateObject = new Date(+dateArray[2], +dateArray[1], +dateArray[0]);
        return dateObject;
    }
    convertStringToDate(dateString: string, dateFormat: string) {
        return moment(dateString, dateFormat).toDate();
    }
    convertDateToString(dateObject, inputFormat) {
        const datestring = moment(dateObject).format(inputFormat);
        return datestring;
    }
    convertMinutesToMilliSec(minutes: number) {
        const milliSec = minutes * 60000;
        return milliSec;
    }
    /**
     * @example
     * for relative path navigation
     * route: ActivatedRoute;
     * this._router.navigateForward(['../patient', this.patient.patientDetails.name], { relativeTo: this.route });
     *
     * current is of absolute path navigation
     */
}
