import { Routes } from '@angular/router';
import { AuthLoginGuard } from 'src/app/shared/core/services/auth-login.guard'; import { CoreModule } from 'src/app/shared/core/core.module';
import { MaterialModule } from 'src/app/shared/widgets_module/material/material.module';
import { IonicModule } from '@ionic/angular';
import { HomePage } from "./home.page";
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { AppointmentsService } from './services/appointments.service';
import { AppointmentComponent } from './appointment/appointment.component';
import { AppointmentService } from './services/appointment.service';
import { DateTimeModule } from 'src/app/shared/widgets_module/date-time/date-time.module';
import { MessagingService } from 'src/app/shared/core/services/messaging.service';
import { OnlineUsersComponent } from './online-users/online-users.component';
import { OnlineUsersService } from './services/online-users.service';
import { AppointmentsPageModule } from '../appointments/appointments.module';
import { RefundComponent } from './appointment/refund/refund.component';
import { MessagesSentSoFarComponent } from './appointment/messages-sent-so-far/messages-sent-so-far.component';

export const componentDeclarations: any[] = [AppointmentListComponent, AppointmentComponent, HomePage,
    OnlineUsersComponent, RefundComponent, MessagesSentSoFarComponent];

export const providerDeclarations: any[] = [AppointmentsService, AppointmentService, MessagingService, OnlineUsersService];
export const modulesImports: any[] = [MaterialModule, IonicModule, CoreModule, DateTimeModule, AppointmentsPageModule];
export const entryComponents: any[] = [OnlineUsersComponent, RefundComponent, MessagesSentSoFarComponent];

export const routes: Routes = [
    {
        path: '', component: AppointmentListComponent, children: [
            { path: '', redirectTo: '', pathMatch: 'full' },
            // { path: 'home', component: AppointmentListComponent, canActivate: [AuthLoginGuard] },
            // { path: 'patient/:id', loadChildren: 'src/app/feature-modules/patient-summary/patient-summary.module#PatientSummaryPageModule',
            //   canLoad: [AuthLoginGuard] }
        ]
    }
];
