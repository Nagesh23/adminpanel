import { Component, OnInit } from '@angular/core';
import { AppointmentData, AppointmentsResponse } from 'src/app/shared/models/appointment_data';
import { AppointmentsService } from '../services/appointments.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/core/services/common.service';
import { MessagingService, PushNotificationData, NotificationOrigin } from 'src/app/shared/core/services/messaging.service';
import { Subscription, Observable, interval } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { OnlineUsersComponent } from '../online-users/online-users.component';
import { OnlineUsersService } from '../services/online-users.service';
import { OnlineUsersData, OnlineUsersResponseModel } from 'src/app/shared/models/online-users_response';
import { map } from 'rxjs/operators';
import { DateTransformerPipe } from 'src/app/shared/widgets_module/date-time/date-transformer/date-transformer.pipe';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss'],
})
export class AppointmentListComponent implements OnInit {
    appointments: AppointmentData[];
    onlinePhys: OnlineUsersData;
    onlinePats: OnlineUsersData;
    today: Date = new Date();
    loader: boolean = false;
  	dateSrchForm: FormGroup;
    minDate: Date;
    totalCount = 10;
    subscrn: Subscription = new Subscription();
    
    auto_refresh$: Subscription;

    constructor(private _appointmentsSer: AppointmentsService, private _fb: FormBuilder, public comSer: CommonService,
        private _notification: MessagingService, private modalCon: ModalController, private _dateTransformer: DatePipe,
        private onUseSer: OnlineUsersService, ) {
		this.minDate = new Date();
		this.dateSrchForm = this._fb.group({
			fromDate: [new Date()],
			toDate: [new Date()],
			patientName: [''], 
			quickFilterCheck: [],
			appointmentStatusList: [],
			limit: []
        });
    }
    ionViewDidEnter() {
        // by default this appo status will be visible only
        // this.setDefaultAppointmentStatus();
        this.syncAppointmentsInList();
        this.refreshOnlineUsers();
    }
    setDefaultAppointmentStatus() {
        this.myAppointments.appointmentStatusList = [4,5, 7, 8, 10, 12];
    }
    ngOnInit() {
        this.initializeAutoRefresh(60000);
        this.subscrn.add(this._notification.currentMessage.subscribe((data: PushNotificationData) => {
            console.log(data);
            console.log('push notification came for appointment');
            if (data.data.parentType === NotificationOrigin.APPOINTMENT) {
                this.syncAppointmentsInList();
            }
        }));
    }
    initializeAutoRefresh(duration: number) {
        console.log(duration);
        if(this.auto_refresh$) {
            this.auto_refresh$.unsubscribe();
            this.auto_refresh$ = null;
        }
        this.auto_refresh$ = interval(duration).pipe(map((value) => {
            console.log('came here for refresh');
            return this.syncAppointmentsInList();
        })).subscribe();
    }
    trackAppointmentById<T>(index: number, item: AppointmentData): any {
        return item.appointmentId;
    }
    initialiseMyAppointmentsJson() {
        this.myAppointments = {
            physicianId: '',
            clinicId: '',
            patientName: '',
            appointmentStatusList: [4,5, 7, 8, 10, 12],
            clinicIdList: [],
            physicianIdList: [],
            fromDate: '',
            toDate: '',
            offset: 0,
            limit: 10
        }
    }
    syncAppointmentsInList() {
        this.loader = false;
        this.comSer.showLoader();
        console.log(this.myAppointments);
        this._appointmentsSer.getAppointmentList(this.myAppointments).subscribe((res: AppointmentsResponse) => {
            this.appointments = res.data ? res.data : [];
            this.comSer.hideLoader();
            this.totalCount = res.totalCount;
            this.loader = true;
            this.setAppointmentStatus();
        });
    }
    refreshOnlineUsers() {
        this.getOnlinePhysicianUsers();
        this.getOnlinePatientsUsers();
    }
    getOnlinePhysicianUsers() {
        this.loader = false;
        this.comSer.showLoader();
        this.onUseSer._getOnlineUsersByRoleForAdminUser(1).subscribe((res: OnlineUsersData) => {
            this.onlinePhys = res;
            this.comSer.hideLoader();
            this.loader = true;
        });
    }
    getOnlinePatientsUsers() {
        this.loader = false;
        this.comSer.showLoader();
        this.onUseSer._getOnlineUsersByRoleForAdminUser(2).subscribe((res: OnlineUsersData) => {
            this.onlinePats = res;
            this.comSer.hideLoader();
            this.loader = true;
        });
    }
    setAppointmentStatus() {
        if(this.appointments) {
            this.appointments.filter((value) => {
                if(value && value.appointmentStatus) {
                    switch(value.appointmentStatus) {
                        case 1: 
                            //NA
                            value.appointmentStatusName = 'Initiated';
                            break;
                        case  2:
                            //NA
                            value.appointmentStatusName = 'Accepted';
                            break;
                        case  3:
                            //NA
                            value.appointmentStatusName = 'Visit Clinic';
                            break;
                        case  4:
                            // rename to 'waiting room' --raghu
                            value.appointmentStatusName = 'Waiting Room';
                            break;
                        case  5:
                            value.appointmentStatusName = 'Cancelled';
                            break;
                        case  6:
                            //NA 
                            value.appointmentStatusName = 'Expired';
                            break;
                        case  7:
                            value.appointmentStatusName = 'Re scheduled';
                            break;
                        case  8:
                            value.appointmentStatusName = 'Completed';
                            break;
                        case  9:
                            //NA
                            value.appointmentStatusName = 'Emergency Visit'; 
                            break;
                        case  10:
                            // Clinic Visited  --- Raghu
                            value.appointmentStatusName = 'In Queue';
                            break;
                        case  11:
                            value.appointmentStatusName = 'Appointment Requested';
                            break;
                        case  12:
                            // Paid Appointment
                            value.appointmentStatusName = 'Waiting Room';
                            break;
                    }
                }
            });
        }
    }
    openOverlay(open: number) {
        if(open === 1) {
            this.modalCon.create({
                component: OnlineUsersComponent,
                componentProps: { data: { flow: open, userData: this.onlinePhys } }
            }).then((e) => {
                e.present();
            });
        } else {
            this.modalCon.create({
                component: OnlineUsersComponent,
                componentProps: { data: { flow: open, userData: this.onlinePats } }
            }).then((e) => {
                e.present();
            });
        }
    }
    getFilteredAppointmentsByAdminUser() {
        this.myAppointments.patientName = this.dateSrchForm.get('patientName').value;
        this.myAppointments.appointmentStatusList = this.dateSrchForm.get('appointmentStatusList').value;
        if(!this.myAppointments.appointmentStatusList) {
            console.log('came here');
            this.myAppointments.appointmentStatusList = [4, 5, 7, 8, 10, 12];
        }
        this.syncAppointmentsInList();
    }
    getDate(value, to) {
        if(to === 1) {
            this.myAppointments.fromDate = value;
        } else {
            this.myAppointments.toDate = value;
        }
        this.filterAppointments();
    }
    getNextList(event) {
		this.myAppointments.offset = event.pageIndex * this.myAppointments.limit;
		this.getFilteredAppointmentsByAdminUser();
    }
    filterAppointments() {
        const fromDate = this.myAppointments.fromDate;
        const toDate = this.myAppointments.toDate;
        if (fromDate) {
            this.dateSrchForm.get('toDate').setValidators(Validators.required);
            this.dateSrchForm.get('toDate').updateValueAndValidity();
        } else {
            this.dateSrchForm.get('fromDate').clearValidators();
            this.dateSrchForm.get('fromDate').updateValueAndValidity();
        }
        if (toDate) {
            this.dateSrchForm.get('fromDate').setValidators(Validators.required);
            this.dateSrchForm.get('fromDate').updateValueAndValidity();
        }
    }
    resetSearchForm() {
        this.dateSrchForm.get('fromDate').clearValidators();
        this.dateSrchForm.get('toDate').clearValidators();
		this.dateSrchForm.reset();
		this.initialiseMyAppointmentsJson();
        // this.getFilteredAppointmentsByAdminUser();
        this.syncAppointmentsInList();
    }

    myAppointments = {
        physicianId: '',
        clinicId: '',
        patientName: '',
        appointmentStatusList: [4,5, 7, 8, 10, 12],
        clinicIdList: [],
        physicianIdList: [],
        fromDate: this._dateTransformer.transform(new Date(), 'DD-MM-YYYY', 'DD-MM-YYYY'),
        toDate: this._dateTransformer.transform(new Date(), 'DD-MM-YYYY', 'DD-MM-YYYY'),
        offset: 0,
        limit: 10
    }
    // Paid App, Booked = Waiting Room   --- raghu
    // clinic visited = In Queue
    appoinStatusList = [
        {
            id : 'None',
            value : '',
            selected : false
        },
        {
            id : 'Accepted',
            value : '2',
            selected : false
        },
        {
            id : 'Expired',
            value : '6',
            selected : false
        },
        {
            id : 'Visit Emergency',
            value : '9',
            selected : false
        },
        {
            id : 'Request Appointment',
            value : '11',
            selected : false
        }
    ]
}



/*
 curl -X POST -H "Authorization: key=AIzaSyBUbqGjkWw84CXQmA2ccFcuvdMl67SfrRc" -H "Content-Type: application/json" -d '{
"data": {
    "title": "Push Notifications RECEIVED",
    "body": "Hey, Hello SYNTAGI",
    "click_action": "https://syntagi.healthcare"
},
"to": "token received cvDFb4J2TO1qsdObwhtndp:APA91bEl_BuuZpNyTYHPKB_0xCNLSM707LhUVWu76pkq8gtOTOBN9w_br-rXHTVH-ZWsVuILGY3EMmID97L_9XpMkOKgbZjh6-Plpi3VjzB7Xl8uycPqxR8BbtJp8_fcVLt_uBOgfuh1"
}' "https://fcm.googleapis.com/fcm/send"
 */