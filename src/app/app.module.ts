import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Routes, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './shared/core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginPageModule } from './login/login.module';
import { AlertModule } from './shared/widgets_module/alert/alert.module';
import { IonicStorageModule } from '@ionic/storage';
import { LandingModule } from './landing/landing.module';
import { HomePageModule } from './feature-modules/home/home.module';
import { HeaderSidebarModule } from './shared/widgets_module/header-sidebar/header-sidebar.module';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { DoctorsPageModule } from './feature-modules/doctors/doctors.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


var firebase = {
      apiKey: "AIzaSyCSmUJ3WQKADAsqr3XfRM2u4vGSxl94cQU",
      authDomain: "syntagi-kanif.firebaseapp.com",
      databaseURL: "https://syntagi-kanif.firebaseio.com",
      projectId: "syntagi-kanif",
      storageBucket: "syntagi-kanif.appspot.com",
      messagingSenderId: "224228122263"
  }

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, BrowserAnimationsModule, AlertModule, CoreModule.forRoot(),
        AppRoutingModule, IonicStorageModule.forRoot(), IonicModule.forRoot(),LoginPageModule, 
        LandingModule, HomePageModule, HeaderSidebarModule, DoctorsPageModule,
        AngularFireAuthModule, AngularFireMessagingModule, AngularFireDatabaseModule,
		AngularFireModule.initializeApp(firebase),
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
