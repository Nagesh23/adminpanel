import { Component, OnInit } from '@angular/core';
import { MessagingService } from './shared/core/services/messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(private messaging: MessagingService) {
  }
  deleteToken() {
    this.messaging.deleteToken();
  }
  ngOnInit() {}
}
