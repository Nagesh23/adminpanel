import { HttpRequestService } from 'src/app/shared/core/services/http-request.service';
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IonRoutingService } from 'src/app/shared/core/services/ionic-routing.service';
import { CommonService } from 'src/app/shared/core/services/common.service';
import { LoginResponseModel } from 'src/app/shared/models/login_response';
// import { AdminUserOnLoadService } from 'src/app/shared/core/services/adminUser-on-load.service';
import { HttpURLS } from 'src/app/shared/models/api_urls';

@Injectable()
export class LoginService {

    constructor(private _httpSer: HttpRequestService, private zone: NgZone,
        public comSer: CommonService, private _router: IonRoutingService) { }

    isLogin() { if (this.comSer.authToken) this._router.navigateRoot(['/adminuser']); }
    validateAdminUser(body: any): Observable<boolean> {
        this.comSer.showLoader();
        return this._httpSer.postResponse<LoginResponseModel>(LoginResponseModel, HttpURLS.VALIDATE_ADMIN_USER_4_LOGIN, body)
        .pipe(map((res: LoginResponseModel) => {
            if (!res.error) {
                this.comSer.adminUser = res.data.adminUserData;
                this.comSer.authToken = res.data.token;
                return true;
            } else {
                this.comSer.runAlert({ status: true, title: 'Login Failed', description: res.message });
                return false;
            }
        }));
    }
    postLogin() {
        this.comSer.setLoginState(true); 
        this._router.navigateRoot(['/adminuser']); 
        this._loadPrePrescnData();
        this.comSer.runAlert({ status: false, title: 'Login Successfully', description: '' });
    }
    private _loadPrePrescnData() {
        this.zone.runOutsideAngular(() => {
        });
    }
}
