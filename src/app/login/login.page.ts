import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './services/login.service';
import { stringForbiddenValidator } from '../shared/core/directives/string_forbidden.directive';
import { MessagingService } from '../shared/core/services/messaging.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [LoginService]
})
export class LoginPage implements OnInit {

    loginFormGP: FormGroup;
    
    constructor(private _fb: FormBuilder, private _loginSer: LoginService,
      private _notification: MessagingService) {}
  
    ngOnInit() {
        this._loginSer.isLogin();
        this.loginFormGP = this._fb.group({
            login: ['', [Validators.required]],
            pin: ['', [Validators.required, stringForbiddenValidator(), Validators.maxLength(4)]],
            sourceType: [1],
            adminUserRoleType: ['', Validators.required]
        });
    }
    validateAdminUser() {
        this._loginSer.validateAdminUser(this.loginFormGP.value).
        subscribe((res: boolean) => {
            if(res) {
                // messaging is not required 
                // this._notification.requestPermission();
                this._loginSer.postLogin();
            }
        }, (error: Error) => { });
    }
}
