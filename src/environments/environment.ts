// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // baseUrl: 'http://13.127.133.104:8082',
  // snomedUrl: 'https://www.loki.syntagi.healthcare'
  baseUrl: 'https://backend.doctorsforseva.org',
  // baseUrl: 'https://jarvis.syntagi.healthcare',
  // snomedUrl: 'https://www.loki.syntagi.healthcare'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
