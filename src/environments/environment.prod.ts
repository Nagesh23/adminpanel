export const environment = {
  production: true,
  // baseUrl: 'http://13.127.133.104:8082',
  // snomedUrl: 'https://www.loki.syntagi.healthcare'

  baseUrl: 'https://backend.doctorsforseva.org',

  // baseUrl: 'https://jarvis.syntagi.healthcare',
  // snomedUrl: 'https://www.loki.syntagi.healthcare'
};
